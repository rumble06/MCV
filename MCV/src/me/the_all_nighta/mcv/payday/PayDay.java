package me.the_all_nighta.mcv.payday;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import me.the_all_nighta.mcv.Database;
import me.the_all_nighta.mcv.MCV;
import me.the_all_nighta.mcv.MainAPI;

public class PayDay implements CommandExecutor, Listener{
	
	private static PayDay instance;
	
	private PayDay() {}
	
	public static PayDay getInstance() {
		if(instance == null) instance = new PayDay();
		return instance;
	}
	
	MainAPI mainApi;
	HashMap<String, Integer> players = new HashMap<String, Integer>();
	HashMap<String, Integer> orejucate = new HashMap<String, Integer>();
	
	public void setupPayDay() {
		mainApi = MCV.mainApi;
		runEveryMinute();
		hourCheck();
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("ore") && sender instanceof Player) {
			Player player = (Player) sender;
			if(args.length==0) {
				String message = "�3[MCV] �fOrele tale jucate: �a"+orejucate.get(player.getName().toLowerCase())+"�f.";
				switch(mainApi.getPermission().getPrimaryGroup(player)) {
				case "Newbie":
					message+="\n�3[MCV] �fMai ai nevoie de �a"+String.valueOf(24-orejucate.get(player.getName().toLowerCase()))+" ore�f pentru a promova la gradul de �aMember�f!";
					break;
				case "Member":
					message+="\n�3[MCV] �fMai ai nevoie de �a"+String.valueOf(360-orejucate.get(player.getName().toLowerCase()))+" ore�f pentru a promova la gradul de �aAdvanced�f!";
					break;
				default:
					break;
				}
				player.sendMessage(message);
			}
			else if(args.length==1) {
				if(!player.hasPermission("mcv.ore.others")) {
					player.sendMessage("�3[MCV] �cTrebuie sa fi �aMember�c pentru a folosi aceasta comanda!");
					return true;
				}
				Player target = MCV.getPlugin().getServer().getPlayer(args[0]);
				if(target==null) {
					player.sendMessage("�3[MCV] �cNu a fost gasit nici un player online cu acest nume!");
					return true;
				}
				String message = "�3[MCV] �fOrele jucate ale lui �7"+target.getName()+"�f: �a"+orejucate.get(target.getName().toLowerCase())+"�f.";
				switch(mainApi.getPermission().getPrimaryGroup(target)) {
				case "Newbie":
					message+="\n�3[MCV] �fMai are nevoie de �a"+String.valueOf(24-orejucate.get(target.getName().toLowerCase()))+" ore�f pentru a promova la gradul de �aMember�f!";
					break;
				case "Member":
					message+="\n�3[MCV] �fMai are nevoie de �a"+String.valueOf(360-orejucate.get(target.getName().toLowerCase()))+" ore�f pentru a promova la gradul de �aAdvanced�f!";
					break;
				default:
					break;
				}
				player.sendMessage(message);
			}
			else {
				player.sendMessage("�3[MCV] �cCorrect syntax: �a/ore [player]�c!");
			}
			return true;
		}
		return false;
	}
	
	@EventHandler
	public void onPlayerLogin(PlayerJoinEvent e) {
		if(orejucate.containsKey(e.getPlayer().getName().toLowerCase())) return;
		try(Connection con = Database.getHikari().getConnection()) {
			PreparedStatement ps = con.prepareStatement("SELECT `ore` FROM `PAYDAY_ORE` WHERE `player` = ?");
			ps.setString(1, e.getPlayer().getName().toLowerCase());
			ResultSet rs = ps.executeQuery();
			int ore = 0;
			if(rs.next())
				ore = rs.getInt("ore");
			orejucate.put(e.getPlayer().getName().toLowerCase(), ore);
		} catch(SQLException er) {
			er.printStackTrace();
			e.getPlayer().kickPlayer("�cAi fost dat afara deoarece a aparut o eroare la incarcarea datelor tale! Te rugam sa incerci sa reintri pe server!");
		}
	}
	
	public void hourCheck() {
		Calendar calendar = Calendar.getInstance();
		Timer timer = new Timer();
		TimerTask hourlyTask = new TimerTask () {
		    @Override
		    public void run () {
		        givePayDay();
		    }
		};
		timer.scheduleAtFixedRate(hourlyTask, millisToNextHour(calendar), 1000*60*60);
	}
	
	public void runEveryMinute() {
		MCV.getPlugin().getServer().getScheduler().runTaskTimer(MCV.getPlugin(), new Runnable() {
			public void run() {
				for(Player p : MCV.getPlugin().getServer().getOnlinePlayers()) {
					int a = 0;
					if(players.containsKey(p.getName().toLowerCase())) a = players.get(p.getName().toLowerCase());
					players.put(p.getName().toLowerCase(), a+1);
				}
			}
		}, 19L*60, 19L*60);
	}
	
	public void givePayDay() {
		MCV.getPlugin().getServer().getScheduler().runTask(MCV.getPlugin(), new Runnable() {
			public void run() {
				for(Player p : MCV.getPlugin().getServer().getOnlinePlayers()) {
					if(players.containsKey(p.getName().toLowerCase())) {
						if(players.get(p.getName().toLowerCase()) >= 25) {
							double money = 16.6;
							money = money*players.get(p.getName().toLowerCase());
							mainApi.getEconomy().depositPlayer(p, money);
							p.sendMessage("�3[PayDay] �fAi primit �a$"+money+"�f!");
							p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, SoundCategory.MASTER, 1, 1);
							players.remove(p.getName().toLowerCase());
							orejucate.put(p.getName().toLowerCase(), orejucate.get(p.getName().toLowerCase()) + 1);
						}
						else {
							p.sendMessage("�3[PayDay] �cNu ai avut destul timp jucat ca sa primesti �aPayDay�c!");
						}
					}
				}
				checkRankUp();
				oreJucateHandler();
			}
		});
	}
	
	public void oreJucateHandler() {
		MCV.getPlugin().getServer().getScheduler().runTaskAsynchronously(MCV.getPlugin(), new Runnable() {
			public void run() {
				try(Connection con = Database.getHikari().getConnection()) {
					PreparedStatement ps = con.prepareStatement("INSERT INTO `PAYDAY_ORE` (`player`, `ore`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `ore` = VALUES(`ore`)");
					for(String s : orejucate.keySet()) {
						ps.setString(1, s);
						ps.setInt(2, orejucate.get(s));
						ps.addBatch();
					}
					ps.executeBatch();
					clearHashMapOJ();
				} catch(SQLException e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void clearHashMapOJ() {
		MCV.getPlugin().getServer().getScheduler().runTask(MCV.getPlugin(), new Runnable() {
			public void run() {
				Iterator<Entry<String, Integer>> ojir = orejucate.entrySet().iterator();
				while(ojir.hasNext()) {
					Entry<String, Integer> ojentry = ojir.next();
					if(MCV.getPlugin().getServer().getPlayerExact(ojentry.getKey()) == null) ojir.remove();
				}
			}
		});
	}
	
	public void checkRankUp() {
		for(Player p : MCV.getPlugin().getServer().getOnlinePlayers()) {
			String pName = p.getName().toLowerCase();
			if(orejucate.containsKey(pName))
				switch(mainApi.getPermission().getPrimaryGroup(p)) {
				case "Newbie":
					if(orejucate.get(pName) >= 24) {
						mainApi.getPermission().playerAddGroup(p, "member");
						p.sendMessage("�2[RankUp] �fAi facut �a24 de ore�f jucate si ai primit rankup la �aMember�f!");
					}
					break;
				case "Member":
					if(orejucate.get(pName) >= 360) {
						mainApi.getPermission().playerAddGroup(p, "advanced");
						p.sendMessage("�2[RankUp] �fAi facut �a360 de ore�f jucate si ai primit rankup la �aAdvanced�f!");
					}
					break;
				default:
					break;
				}
		}
	}
	
	private static long millisToNextHour(Calendar calendar) {
	    int minutes = calendar.get(Calendar.MINUTE);
	    int seconds = calendar.get(Calendar.SECOND);
	    int millis = calendar.get(Calendar.MILLISECOND);
	    int minutesToNextHour = 60 - minutes;
	    int secondsToNextHour = 60 - seconds;
	    int millisToNextHour = 1000 - millis;
	    return minutesToNextHour*60*1000 + secondsToNextHour*1000 + millisToNextHour;
	}

	
}
