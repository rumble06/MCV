package me.the_all_nighta.mcv.shop;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class NPCAddItem implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("shopadd") && sender instanceof Player) {
			
			Player player = (Player) sender;
			if(!player.hasPermission(cmd.getPermission())) {
				player.sendMessage("�3[MCV] �cNu ai acces la aceasta comanda!");
				return true;
			}
			if(args.length!=1) {
				player.sendMessage("�3[MCV] �cSintaxa corecta: �a"+cmd.getUsage()+"\n�3[MCV] �cDescriere comanda: �a"+cmd.getDescription());
				return true;
			}
			double price = 0;
			try {
				price = Double.parseDouble(args[0]);
			} catch(NumberFormatException e){
				player.sendMessage("�3[MCV] �cSintaxa corecta: �a"+cmd.getUsage()+"\n�3[MCV] �cDescriere comanda: �a"+cmd.getDescription());
				return true;
			}
			if(player.getInventory().getItemInMainHand() == null || player.getInventory().getItemInMainHand().getType().equals(Material.AIR)) {
				player.sendMessage("�3[MCV] �cNu ai nici un item in mana pentru a vinde!");
				return true;
			}
			
			ItemStack item = player.getInventory().getItemInMainHand();
			
			
			
			return true;
		}
		
		return false;
	}
	
	
}