package me.the_all_nighta.mcv.shop;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import me.the_all_nighta.mcv.MCV;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;

public class NPCCreate implements CommandExecutor {
	
	NPCDataAPI NPC_API = new NPCDataAPI();
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("createshop") && sender instanceof Player) {
			
			Player player = (Player) sender;
			if(!player.hasPermission(cmd.getPermission())) {
				player.sendMessage("�3[MCV] �cNu ai acces la aceasta comanda!"+cmd.getUsage());
				return true;
			}
			
			if(args.length!=2) {
				player.sendMessage("�3[MCV] �cSintaxa corecta: �a"+cmd.getUsage());
				return true;
			}
			
			String shopID = args[0].toLowerCase();
			String priceString = args[1];
			double price = -1;
			try {
				price = Double.parseDouble(priceString);
			} catch(NumberFormatException e) {
				player.sendMessage("�3[MCV] �cSintaxa corecta: �a");
				return true;
			}
			
			if(NPC_API.getNPC(shopID) != null) {
				player.sendMessage("�3[MCV] �cNPC-ul cu acest ID exista deja!");
				return true;
			}
			
			NPC CitizenNPC = CitizensAPI.getNamedNPCRegistry("shops").createNPC(EntityType.VILLAGER, "SHOP");
			CitizenNPC.spawn(player.getLocation());
			NPC_API.createNPCdata(shopID, CitizenNPC.getUniqueId(), price, MCV.GetServer().createInventory(null, 9*6, "�aSHOP ID: #"+shopID));
			
			return true;
		}
		return false;
	}
	
	
}
