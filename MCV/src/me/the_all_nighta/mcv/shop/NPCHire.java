package me.the_all_nighta.mcv.shop;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.the_all_nighta.mcv.MCV;
import net.milkbowl.vault.economy.Economy;

public class NPCHire implements CommandExecutor {
	
	NPCDataAPI NPC_API = new NPCDataAPI();

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("buyshop") && sender instanceof Player) {
			
			Player player = (Player) sender;
			if(!player.hasPermission(cmd.getPermission())) {
				player.sendMessage("�3[MCV] �cNu ai acces la aceasta comanda!");
				return true;
			}
			
			if(NPC_API.getNPC(player) != null) {
				player.sendMessage("�3[MCV] �cAi deja un SHOP inchiriat!");
				return true;
			}
			
			if(args.length != 1) {
				player.sendMessage("�3[MCV] �cSintaxa corecta: �f<�a"+cmd.getUsage()+"�f>�c!");
				return true;
			}
			
			NPCDataHolder npc = NPC_API.getNPC(args[0]);
			if(npc.getOwner() != null) {
				player.sendMessage("�3[MCV] �cAcest NPC este deja inchiriat!");
				return true;
			}
			if(npc.getPrice()<0) {
				player.sendMessage("�3[MCV] �fAcest SHOP este premium. Pentru a inchiria acest NPC contacteaza unul dintre fondatori!");
				return true;
			}
			Economy econ = MCV.getMainAPI().getEconomy();
			if(econ.getBalance(player) < npc.getPrice()) {
				player.sendMessage("�3[MCV] �cNu ai destui bani sa cumperi acest NPC! Acesta costa �a$"+npc.getPrice()+"�c!");
				return true;
			}
			
			npc.setOwner(player, System.currentTimeMillis()/1000, (System.currentTimeMillis()/1000)+(60*60*24*7));
			econ.withdrawPlayer(player, npc.getPrice());
			player.sendMessage(String.format("�3[MCV] �fFelicitari, ai cumparat SHOP-ul cu id-ul �a%s�f pentru pretul de �a%s�f!", npc.getID(), npc.getPrice()));
			
			return true;
			
		}
		
		return false;
	}

}
