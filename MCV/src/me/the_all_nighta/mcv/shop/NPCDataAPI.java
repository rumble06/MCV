package me.the_all_nighta.mcv.shop;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;


public class NPCDataAPI {
	
	private static ArrayList<NPCDataHolder> npcs = new ArrayList<NPCDataHolder>();
	
	public NPCDataHolder getNPC(String by, String arg) {
		for(NPCDataHolder npc : npcs) {
			if(by.equalsIgnoreCase("id") && arg.equalsIgnoreCase(npc.getID())) return npc;
			if(by.equalsIgnoreCase("uuid") && arg.equalsIgnoreCase(npc.getUUID())) return npc;
			if(by.equalsIgnoreCase("owner") && arg.equalsIgnoreCase(npc.getOwner())) return npc;
		}
		return null;
	}
	
	public NPCDataHolder getNPC(String id) {
		return getNPC("id", id);
	}
	
	public NPCDataHolder getNPC(UUID uuid) {
		return getNPC("uuid", uuid.toString());
	}
	
	public NPCDataHolder getNPC(Player owner) {
		return getNPC("owner", owner.getName());
	}
	
	public NPCDataHolder createNPCdata(String id, String uuid, double price, Inventory inventory) {
		npcs.add(new NPCDataHolder(id, uuid, price, inventory));
		return getNPC("id", id);
	}
	
	public NPCDataHolder createNPCdata(String id, UUID uuid, double price, Inventory inventory) {
		return createNPCdata(id, uuid.toString(), price, inventory);
	}
	
}
