package me.the_all_nighta.mcv.shop;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.event.NPCClickEvent;

public class NPCClick implements Listener {
	
	NPCDataAPI NPC_API = new NPCDataAPI();
	
	@EventHandler
	public void onNPCInteract(NPCClickEvent e) {
		if(!e.getClicker().hasPermission("mcv.shop.open")) return;
		if(e.getNPC().getOwningRegistry().equals(CitizensAPI.getNamedNPCRegistry("shops")) && NPC_API.getNPC(e.getNPC().getUniqueId()) != null) {
			NPCDataHolder npc = NPC_API.getNPC(e.getNPC().getUniqueId());
			if(npc.getOwner() == null) {
				if(npc.getPrice()>=0) {
					e.getClicker().sendMessage("�3[MCV] �fAcest SHOP costa �a"+npc.getPrice()+"�f. Pentru a inchiria acest NPC foloseste comanda <�a/buyshop "+npc.getID()+"�f>!");
				}
				else {
					e.getClicker().sendMessage("�3[MCV] �fAcest SHOP este premium. Pentru a inchiria acest NPC contacteaza unul dintre fondatori!");
				}
				return;
			}
			e.getClicker().openInventory(npc.getInventory());
		}
	}

}
