package me.the_all_nighta.mcv.shop;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class NPCDataHolder {
	
	private String id;
	private String uuid;
	private String owner;
	private double price;
	private Inventory inventory;
	private long dateHired;
	private long hiredFor;
	
	public NPCDataHolder(String id, String uuid, double price, Inventory inventory) {
		this.id = id.toLowerCase();
		this.uuid = uuid.toLowerCase();
		this.price = price;
		this.inventory = inventory;
	}
	
	public String getID() {
		return id;
	}
	
	public String getUUID() {
		return uuid;
	}
	
	public String getOwner() {
		return owner;
	}
	
	public double getPrice() {
		return price;
	}
	
	public Inventory getInventory() {
		return inventory;
	}
	
	public long getDateHired() {
		return dateHired;
	}
	
	public long getHiredFor() {
		return hiredFor;
	}
	
	public void setID(String id) {
		this.id = id.toLowerCase();
	}
	
	public void setUUID(String uuid) {
		this.uuid = uuid.toLowerCase();
	}
	
	public void setOwner(String owner, long dateHired, long hiredFor) {
		this.owner = owner.toLowerCase();
		this.dateHired = dateHired;
		this.hiredFor = hiredFor;
	}
	
	public void setOwner(Player owner, long dateHired, long hiredFor) {
		setOwner(owner.getName(), dateHired, hiredFor);
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}
	
	
	public boolean hasOwner() {
		return !owner.isEmpty();
	}
	
	public double getItemPrice(ItemStack is) {
		for(ItemStack isInv : getInventory().getContents()) {
			if(is.getType().equals(isInv.getType()) && is.getItemMeta().equals(isInv.getItemMeta())) {
				ItemMeta meta = is.getItemMeta();
				String priceString = meta.getLore().get(0).substring(meta.getLore().get(0).lastIndexOf(" ")+1, meta.getLore().get(0).indexOf("$"));
				double price = -1;
				try {
					price = Double.parseDouble(priceString);
				} catch(NumberFormatException e) {}
				return price;
			}
		}
		return -1;
	}
	
	public boolean isFull() {
		return getInventory().firstEmpty() == -1 ? true : false;
	}
	
	public int getItemAmount(ItemStack is) {
		for(ItemStack isInv : getInventory().getContents()) {
			if(is.getType().equals(isInv.getType()) && is.getItemMeta().equals(isInv.getItemMeta())) {
				return isInv.getAmount();
			}
		}
		return 0;
	}
	
	

	public void removeItem(ItemStack is, int amount) {
		for(ItemStack isInv : getInventory().getContents()) {
			if(is.getType().equals(isInv.getType()) && is.getItemMeta().equals(isInv.getItemMeta())) {
				isInv.setAmount(isInv.getAmount()-amount);
				return;
			}
		}
	}
	
	public void addItem(ItemStack is, double price) {
		ItemMeta meta = is.getItemMeta();
		List<String> metaLore = meta.getLore();
		metaLore.add(0, "Pret bucata: "+price+"$");
		meta.setLore(metaLore); is.setItemMeta(meta);
		getInventory().addItem(is);
	}
	
}
