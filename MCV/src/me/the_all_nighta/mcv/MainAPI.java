package me.the_all_nighta.mcv;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;

import com.earth2me.essentials.Essentials;
import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.bukkit.BukkitUtil;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.data.DataException;
import com.sk89q.worldedit.schematic.SchematicFormat;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

@SuppressWarnings("deprecation")
public class MainAPI {
	
	public static Economy econ = null;
	boolean worldEditSupport;
	WorldEditPlugin worldEdit;
	ArrayList<Location> schematicsPreventOverload = new ArrayList<Location>();
	private static Chat chat = null;
	public static Essentials ess = null;
	public static Permission permission = null;
	
	public void setupMainAPI()
	{
		setupWorldEdit();
        if (!setupEconomy() ) {
            MCV.getPlugin().getLogger().info("[%s] - Disabled due to no Vault dependency found!");
            MCV.getPlugin().getServer().getPluginManager().disablePlugin(MCV.getPlugin());
            return;
        }
        setupChat();
        setupEssentials();
        setupPermissions();
	}
	
	public Economy getEconomy()
	{
		return econ;
	}
	
	public Chat getChat()
	{
		return chat;
	}
	
	public Permission getPermission() {
		return permission;
	}
	
	public Essentials getEssentials() {
		return ess;
	}
	
	private boolean setupPermissions()
    {
        RegisteredServiceProvider<Permission> permissionProvider = MCV.getPlugin().getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            permission = permissionProvider.getProvider();
        }
        return (permission != null);
    }
	
	private void setupEssentials() {
		ess = (Essentials) MCV.getPlugin().getServer().getPluginManager().getPlugin("Essentials");
	}
	
	 private boolean setupChat() {
	        RegisteredServiceProvider<Chat> rsp = MCV.getPlugin().getServer().getServicesManager().getRegistration(Chat.class);
	        chat = rsp.getProvider();
	        return chat != null;
	 }
	
	private boolean setupEconomy() {
		if (MCV.getPlugin().getServer().getPluginManager().getPlugin("Vault") == null) {
			return false;
		}
		RegisteredServiceProvider<Economy> rsp = MCV.getPlugin().getServer().getServicesManager().getRegistration(Economy.class);
		if (rsp == null) {
			return false;
		}
		econ = rsp.getProvider();
		return econ != null;
	}
	
	public ItemStack setItemTitle(ItemStack item, String title)
	{
		ItemMeta itemMeta = item.getItemMeta(); itemMeta.setDisplayName(title);
		item.setItemMeta(itemMeta);
		return item;
	}
	
	public ItemStack setItemLore(ItemStack item, String lore, int line)
	{
		ItemMeta itemMeta = item.getItemMeta();
		List<String> itemLore = new ArrayList<String>();
		try {itemLore.addAll(itemMeta.getLore());} catch(NullPointerException e) {};
		itemLore.add(line-1, lore);
		itemMeta.setLore(itemLore);
		item.setItemMeta(itemMeta);
		return item;
	}
	
	public ItemStack setItemLores(ItemStack item, List<String> list)
	{
		ItemMeta itemMeta = item.getItemMeta();
		itemMeta.setLore(list);
		item.setItemMeta(itemMeta);
		return item;
	}
	
	public ItemStack deleteItemLore(ItemStack item, int line)
	{
		ItemMeta itemMeta = item.getItemMeta();
		List<String> itemLore = itemMeta.getLore();
		try {
			itemLore.remove(line-1);
			itemMeta.setLore(itemLore);
			item.setItemMeta(itemMeta);
		} catch (NullPointerException e) {}
		return item;
	}
	
	public boolean isInRegion(Block block, String regionName)
	{
		return isInRegion(block.getLocation(), regionName);
	}
	
	public boolean isInRegion(Player player, String regionName)
	{
		return isInRegion(player.getLocation(), regionName);
	}
	
    public boolean isInRegion(Location location, String regionName){
        RegionManager regionManager = getWorldGuard().getRegionManager(location.getWorld());
        ApplicableRegionSet regions = regionManager.getApplicableRegions(location);
        if (regions.size() == 0){
            return false;
        }else{
            for (ProtectedRegion region : regions){
                   if (region.getId().equalsIgnoreCase(regionName)){
                        return true;
                   }
            }
            return false;
        }
    }
	
    private WorldGuardPlugin getWorldGuard() {
        Plugin plugin = MCV.getPlugin().getServer().getPluginManager().getPlugin("WorldGuard");
     
        // WorldGuard may not be loaded
        if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
        	MCV.getPlugin().getLogger().severe("Can't enable plugin because WorldGuard is missing");
        	MCV.getPlugin().getServer().getPluginManager().disablePlugin(MCV.getPlugin());
            return null; // Maybe you want throw an exception instead
        }
     
        return (WorldGuardPlugin) plugin;
    }
    
	private void setupWorldEdit() {
        Plugin plugin = MCV.getPlugin().getServer().getPluginManager().getPlugin("WorldEdit");
        if (plugin == null || !(plugin instanceof WorldEditPlugin)){
        	MCV.getPlugin().getLogger().severe("Plugin disabled because WorldEdit not found");
        	MCV.getPlugin().getServer().getPluginManager().disablePlugin(MCV.getPlugin());
            return;
        }

        worldEditSupport = true;
        worldEdit = (WorldEditPlugin) plugin;
        MCV.getPlugin().getServer().getConsoleSender().sendMessage("Enabled WorldEdit support!");
    }
	
	public int getItemAmount(Player player, Material item)
	{
		return getItemAmount(player, item, 0);
	}
	
	public int getItemAmount(Player player, Material item, int type)
	{
		int amount = 0;
		for(Map.Entry<Integer, ? extends ItemStack> set : player.getInventory().all(item).entrySet())
		{
			if(set.getValue().getDurability() == (short) type)
				amount += set.getValue().getAmount();
		}
		return amount;
	}
	
	public void removeItemAmount(Player player, Material item, int amount)
	{
		removeItemAmount(player, item, amount, 0);
	}
	
	public void removeItemAmount(Player player, Material item, int amount, int type)
	{
		player.getInventory().removeItem(new ItemStack(item, amount, (short) type));
	}
	
	public void pasteSchematicDelay(String schematic, double x, double y, double z, int seconds)
	{
		pasteSchematicDelay(schematic, x, y, z, seconds, false);
	}
	
	public void pasteSchematicDelay(String schematic, double x, double y, double z, int seconds, boolean air)
	{
		Location loc = new Location(MCV.getPlugin().getServer().getWorld("world_mcv"), x, y, z);
		if(schematicsPreventOverload.contains(loc)) return;
		schematicsPreventOverload.add(loc);
		MCV.getPlugin().getServer().getScheduler().runTaskLater(MCV.getPlugin(), new Runnable()
		{
			@Override
			public void run()
			{
				pasteSchematic(schematic+".schematic", loc, air);
				schematicsPreventOverload.remove(loc);
			}
		}, 20L*seconds);
	}
	
	public void pasteSchematic(String schematicName, Location pasteLoc, boolean air) {
	      try {
	          File dir = new File(MCV.getPlugin().getDataFolder(), "/schematics/" + schematicName);

	          EditSession editSession = new EditSession(new BukkitWorld(pasteLoc.getWorld()), 999999999);
	          editSession.enableQueue();

	          SchematicFormat schematic = SchematicFormat.getFormat(dir);
	          CuboidClipboard clipboard = schematic.load(dir);

	          clipboard.paste(editSession, BukkitUtil.toVector(pasteLoc), !air);
	          editSession.flushQueue();
	      } catch (DataException | IOException ex) {
	          ex.printStackTrace();
	      } catch (MaxChangedBlocksException ex) {
	          ex.printStackTrace();
	      }
	  }
	
}
