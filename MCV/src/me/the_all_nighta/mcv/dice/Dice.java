package me.the_all_nighta.mcv.dice;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;
import me.the_all_nighta.mcv.MCV;
import me.the_all_nighta.mcv.MainAPI;

class DataProvoked {
	String p1 = "";
	String p2 = "";
	boolean inMatch = false;
	double money = 0;
	Timestamp timestamp = new Timestamp(System.currentTimeMillis());
}

public class Dice implements CommandExecutor, Listener {
	
	private static Dice instance;
	
	private Dice() {}
	
	public static Dice getInstance() {
		if(instance == null) instance = new Dice();
		return instance;
	}
	
	MainAPI mainApi = MCV.mainApi;
	ArrayList<DataProvoked> dataProvokedHolder = new ArrayList<DataProvoked>();
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("dice"))
		{
			if(!(sender instanceof Player)) return true;
			double money = 0;
			Player player = (Player) sender;
			if(!mainApi.isInRegion(player, "dice"))
			{
				player.sendMessage("�2[Dice] �cTrebuie sa fi in regiunea speciala pentru '�3dice�c' ca sa folosesti aceasta comanda!");
				return true;
			}
			if(args.length!=2)
			{
				player.sendMessage("�2[Dice] �fCorrect syntax: /dice <player> <money>");
				return true;
			}
			try{
				money = Double.parseDouble(args[1]);
			} catch(NumberFormatException e)
			{
				player.sendMessage("�2[Dice] �fCorrect syntax: /dice <player> <money>");
				return true;
			}
			Player player2 = MCV.getPlugin().getServer().getPlayer(args[0]);
			if(player2==null)
			{
				player.sendMessage("�2[Dice] �cPlayerul respectiv nu este online!");
				return true;
			}
			if(!mainApi.isInRegion(player, "dice"))
			{
				player.sendMessage("�2[Dice] �cPlayerul respectiv nu este in regiunea de dice!");
				return true;
			}
			if(money < 500 || money > 10000)
			{
				player.sendMessage("�2[Dice] �cMinimul sumei pentru dice este de�3 500$�c iar maximul este de�3 10000$�c!");
				return true;
			}
			if(mainApi.getEconomy().getBalance(player) < money)
			{
				player.sendMessage("�2[Dice] �cNu ai destui bani!");
				return true;
			}
			if(mainApi.getEconomy().getBalance(player2) < money)
			{
				player.sendMessage("�2[Dice] �cPlayerul respectiv nu are destui bani!");
				return true;
			}
			DataProvoked toadd = new DataProvoked();
			toadd.p1 = player.getName().toLowerCase(); toadd.p2 = player2.getName().toLowerCase(); toadd.money = money;
			dataProvokedHolder.add(toadd);	
			player.sendMessage("�2[Dice] �fAi provocat playerul �3"+player2.getName()+"�f la dice pe suma de �3"+money+"$�f!");
			player2.sendMessage("�2[Dice] �fAi fost provocat de playerul �3"+player.getName()+"�f la dice pe suma de �3"+money+"$�f! Foloseste '�3/adice "+player.getName()+"�f' pentru a accepta provocarea!");
			return true;
		}
		
		else if(cmd.getName().equalsIgnoreCase("adice"))
		{
			if(!(sender instanceof Player)) return true;
			Player player2 = (Player) sender;
			if(!mainApi.isInRegion(player2, "dice"))
			{
				player2.sendMessage("�2[Dice] �cTrebuie sa fi in regiunea speciala pentru '�3dice�c' ca sa folosesti aceasta comanda!");
				return true;
			}
			if(isInMatch(player2))
			{
				player2.sendMessage("�2[Dice] �cNu poti folosi aceasta comanda daca esti deja intr-un meci de dice!");
				return true;
			}
			if(args.length!=1)
			{
				player2.sendMessage("�2[Dice] Correct syntax: /adice <player>");
				return true;
			}
			Player player1 = MCV.getPlugin().getServer().getPlayer(args[0]);
			if(player1 == null)
			{
				player2.sendMessage("�2[Dice] �cPlayerul respectiv nu este online!");
				return true;
			}
			if(!mainApi.isInRegion(player1, "dice"))
			{
				player2.sendMessage("�2[Dice] �cPlayerul respectiv nu este in regiunea de dice!");
				return true;
			}
			if(isInMatch(player1))
			{
				player2.sendMessage("�2[Dice] �cPlayerul respectiv este deja intr-un meci!");
				return true;
			}
			if(!isProvoked(player2, player1))
			{
				player2.sendMessage("�2[Dice] �cNu ai fost provocat la dice de acest player!");
				return true;
			}
			DataProvoked data = getProvoke(player1, player2);
			if(mainApi.getEconomy().getBalance(player1) < data.money)
			{
				player2.sendMessage("�2[Dice] �cPlayerul respectiv nu are destui bani!");
				return true;
			}
			if(mainApi.getEconomy().getBalance(player2) < data.money)
			{
				player2.sendMessage("�2[Dice] �cNu ai destui bani!");
				return true;
			}
			data.inMatch = true;
			replaceProvoke(getProvoke(player1, player2), data);
			removeAllProvokes(player1, false);
			removeAllProvokes(player2, false);
			mainApi.getEconomy().withdrawPlayer(player1, data.money);
			mainApi.getEconomy().withdrawPlayer(player2, data.money);
			String p1s = player1.getName();
			String p2s = player2.getName();
		    new BukkitRunnable() {
		        
		    	int i = 5;
		    	@Override
		    	public void run() {
		    		Player p1 = MCV.getPlugin().getServer().getPlayerExact(p1s);
		    		Player p2 = MCV.getPlugin().getServer().getPlayerExact(p2s);
		    		if(p1==null)
		    		{
		    			p2.sendMessage("�2[Dice] �fPlayerul �3"+p1s+"�f a iesit de pe server asa ca ai fost proclamat automat castigator! Ai primit �3"+data.money+"$�f!");
		    			mainApi.getEconomy().depositPlayer(p2, data.money*2);
		    			removeAllProvokes(p1s, true); removeAllProvokes(p2s, true);
		    			cancel();
		    			return;
		    		}
		    		if(p2 == null)
		    		{
		    			p1.sendMessage("�2[Dice] �fPlayerul �3"+p2s+"�f a iesit de pe server asa ca ai fost proclamat automat castigator! Ai primit �3"+data.money+"$�f!");
		    			mainApi.getEconomy().depositPlayer(p1, data.money*2);
		    			removeAllProvokes(p1s, true); removeAllProvokes(p2s, true);
		    			cancel();
		    			return;
		    		}
		    		if(i<=0)
		    		{
		    			int p1d = ThreadLocalRandom.current().nextInt(1, 7);
		    			int p2d = ThreadLocalRandom.current().nextInt(1, 7);
		    			p1.sendMessage("�2[Dice] �fAi aruncat cu zarul si a picat �3"+p1d+"�f!");
		    			p2.sendMessage("�2[Dice] �fAi aruncat cu zarul si a picat �3"+p2d+"�f!");
		    			p1.sendMessage("�2[Dice] �fPlayerul �3"+p2s+"�f a aruncat cu zarul si a picat �3"+p2d+"�f!");
		    			p2.sendMessage("�2[Dice] �fPlayerul �3"+p1s+"�f a aruncat cu zarul si a picat �3"+p1d+"�f!");
		    			if(p1d>p2d)
		    			{
		    				p1.sendMessage("�2[Dice] �fAi aruncat un numar mai mare cu zarul asa ca ai castigat �3"+data.money+"$�f!");
		    				p2.sendMessage("�2[Dice] �fAi pierdut in fata jucatorului "+p1s+"�f!");
		    				mainApi.getEconomy().depositPlayer(p1, data.money*2);
		    			}
		    			else if(p2d>p1d)
		    			{
		    				p1.sendMessage("�2[Dice] �fAi pierdut in fata jucatorului �3"+p2s+"�f!");
		    				p2.sendMessage("�2[Dice] �fAi aruncat un numar mai mare cu zarul asa ca ai castigat �3"+data.money+"$�f!");
		    				mainApi.getEconomy().depositPlayer(p2, data.money*2);
		    			}
		    			else
		    			{
		    				p1.sendMessage("�2[Dice] �fTi-ai primit banii inapoi deoarece ati aruncat amandoi acelasi numar!");
		    				p2.sendMessage("�2[Dice] �fTi-ai primit banii inapoi deoarece ati aruncat amandoi acelasi numar!");
		    				mainApi.getEconomy().depositPlayer(p1, data.money);
		    				mainApi.getEconomy().depositPlayer(p2, data.money);
		    			}
		    			removeAllProvokes(p1s, true); removeAllProvokes(p2s, true);
		    			cancel();
		    			return;
		    		}
		    		else
		    		{
		    			p1.sendMessage("�2[Dice] �3"+i+"�f secunde pana se va arunca cu zarurile!");
		    			p2.sendMessage("�2[Dice] �3"+i+"�f secunde pana se va arunca cu zarurile!");
		    		}
		    		i--;
		    	}
		     
		    }.runTaskTimer(MCV.getPlugin(), 0l, 20l);
			return true;
		}
		
		return false;
	}
	
	
	public void leaveEvent(Player player)
	{
		removeAllProvokes(player, false);
	}
	
	public void onPlayerKickEvent(PlayerKickEvent e)
	{
		leaveEvent(e.getPlayer());
	}
	
	public void onPlayerQuitEvent(PlayerQuitEvent e)
	{
		leaveEvent(e.getPlayer());
	}
	
	public boolean isInMatch(Player player)
	{
		return isInMatch(player.getName());
	}
	
	public boolean isInMatch(String player)
	{
		for(DataProvoked data : dataProvokedHolder)
			if((data.p1.equals(player.toLowerCase()) || data.p2.equals(player.toLowerCase())) && data.inMatch) return true;
		return false;
	}
	
	public void removeAllProvokes(Player player, boolean includeInMatch)
	{
		removeAllProvokes(player.getName(), includeInMatch);
	}
	
	public void removeAllProvokes(String player, boolean includeInMatch)
	{
		ListIterator<DataProvoked> listitr = dataProvokedHolder.listIterator();
		while(listitr.hasNext())
		{
			DataProvoked data = listitr.next();
			if(data.p1.equalsIgnoreCase(player) || data.p2.equalsIgnoreCase(player)) 
				if( ( data.inMatch && includeInMatch ) || !data.inMatch) listitr.remove();
		}
	}
	
	public boolean isProvoked(Player provoked, Player provoker)
	{
		return isProvoked(provoked.getName(), provoker.getName());
	}
	
	public boolean isProvoked(String provoked, String provoker)
	{
		for(DataProvoked data : dataProvokedHolder)
			if(data.p2.equals(provoked.toLowerCase()) && data.p1.equals(provoker.toLowerCase())) return true;
		return false;
	}
	
	public void replaceProvoke(DataProvoked old_Data, DataProvoked new_Data)
	{
		if(dataProvokedHolder.contains(old_Data)) {
			dataProvokedHolder.remove(old_Data);
			dataProvokedHolder.add(new_Data);
		}
	}
	
	public DataProvoked getProvoke(Player p1, Player p2)
	{
		return getProvoke(p1.getName(), p2.getName());
	}
	
	public DataProvoked getProvoke(String p1, String p2)
	{
		for(DataProvoked data : dataProvokedHolder)
			if(data.p1.equals(p1.toLowerCase()) && data.p2.equals(p2.toLowerCase())) return data;
		return new DataProvoked();
	}
	
	public void expireProvokeCheck()
	{
		MCV.getPlugin().getServer().getScheduler().runTaskTimer(MCV.getPlugin(), new Runnable(){
			public void run()
			{
				ListIterator<DataProvoked> li = dataProvokedHolder.listIterator();
				while(li.hasNext())
				{
					DataProvoked data = li.next();
					if(data.timestamp.after(new Timestamp(System.currentTimeMillis() + 1000*30)) && !data.inMatch)
					{
						li.remove();
					}
				}
			}
		}, 20L, 20L);
	}
}
