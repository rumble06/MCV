package me.the_all_nighta.mcv.adminduty;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.the_all_nighta.mcv.MCV;

public class AdminChat implements CommandExecutor {
	
	private static AdminChat instance;
	
	private AdminChat() {}
	
	public static AdminChat getInstance() {
		if(instance == null)
			instance = new AdminChat();
		return instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("a") && sender instanceof Player) {
			if(!sender.hasPermission("mcv.adminchat")) {
				sender.sendMessage("�3[MCV] �cNu ai acces la aceasta comanda!");
				return true;
			}
			if(args.length==0) {
				sender.sendMessage("�3[MCV] �cSintaxa corecta: �a/a <mesaj>�c!");
				return true;
			}
			Player player = (Player) sender;
			String message = "";
			for(int i=0;i<args.length-1;i++) message = message+args[i]+" "; message+=args[args.length-1];
			for(Player receiver : MCV.getPlugin().getServer().getOnlinePlayers()) {
				if(receiver.hasPermission("mcv.adminchat")) receiver.sendMessage("�3[AdminChat]�8 "+player.getName()+"�f: �6"+message);
			}
			return true;
		}
		
		return false;
	}

}
