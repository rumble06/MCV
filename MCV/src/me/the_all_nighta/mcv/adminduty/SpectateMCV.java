package me.the_all_nighta.mcv.adminduty;

import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

import me.the_all_nighta.mcv.MCV;
import me.the_all_nighta.mcv.MainAPI;

public class SpectateMCV implements CommandExecutor, Listener {
	
	private static SpectateMCV instance;
	
	private SpectateMCV() {}
	
	public static SpectateMCV getInstance() {
		if(instance == null) instance = new SpectateMCV();
		return instance;
	}
	
	MainAPI mainApi;
	//ArrayList<String> spectators = new ArrayList<String>();
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("spectate") && sender instanceof Player) {
			Player player = (Player) sender;
			if(!player.hasPermission("mcv.spectate")) {
				player.sendMessage("�3[MCV] �cNu ai acces la aceasta comanda!");
				return true;
			}
			if(args.length != 1) {
				player.sendMessage("�3[MCV] �fSintaxa corecta: �a/spectate <player>�f");
				return true;
			}
			Player spec = MCV.getPlugin().getServer().getPlayer(args[0]);
			if(spec==null) {
				player.sendMessage("�3[MCV] �cPlayer-ul respectiv nu este online!");
				return true;
			}
			if(!spec.getLocation().getWorld().equals(MCV.getPlugin().getServer().getWorld("world_mcv")) && !mainApi.isInRegion(spec, "pvparena")) {
				player.sendMessage("�3[MCV] �cAcest player nu se afla in harta speciala!");
				return true;
			}
			//spectators.add(player.getName().toLowerCase());
			player.setGameMode(GameMode.ADVENTURE);
			mainApi.getEssentials().getUser(player).setGodModeEnabled(true);
			player.setAllowFlight(true);
			player.teleport(spec);
			return true;
		}
		else if(cmd.getName().equalsIgnoreCase("stopspectate") && sender instanceof Player) {
			Player player = (Player) sender;
			if(!player.hasPermission("mcv.spectate")) {
				player.sendMessage("�3[MCV] �cNu ai acces la aceasta comanda!");
				return true;
			}
			if(!player.getGameMode().equals(GameMode.ADVENTURE)) {
				player.sendMessage("�3[MCV] �cNu esti spectator!");
				return true;
			}
			player.teleport(MCV.getPlugin().getServer().getWorld("world").getSpawnLocation());
			player.setGameMode(GameMode.SURVIVAL);
			player.setAllowFlight(false);
			mainApi.getEssentials().getUser(player).setGodModeEnabled(false);
			//spectators.remove(player.getName().toLowerCase());
			return true;
		}
		return false;
	}
	
	@EventHandler(priority=EventPriority.LOWEST)
	public void onPortalEvent(PlayerPortalEvent e) {
		if(e.getPlayer().getGameMode().equals(GameMode.ADVENTURE)) {
			e.getPlayer().sendMessage("�3[MCV] �cNu poti folosi portalul ca spectator!");
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerInteractEntity(PlayerInteractEntityEvent e) {
		if(e.getPlayer().getGameMode().equals(GameMode.ADVENTURE)) e.setCancelled(true);
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if(e.getPlayer().getGameMode().equals(GameMode.ADVENTURE)) e.setCancelled(true);
	}
	
	@EventHandler
	public void onPlayerLogin(PlayerJoinEvent e) {
		if(e.getPlayer().getGameMode().equals(GameMode.ADVENTURE)) {
			e.getPlayer().setGameMode(GameMode.SURVIVAL);
			e.getPlayer().setAllowFlight(false);
			mainApi.getEssentials().getUser(e.getPlayer()).setGodModeEnabled(false);
		}
	}
	
	@EventHandler
	public void onPlayerPickupItems(EntityPickupItemEvent e) {
		if(e.getEntity() instanceof Player) {
			Player player = (Player) e.getEntity();
			if(player.getGameMode().equals(GameMode.ADVENTURE)) e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerThrowItems(PlayerDropItemEvent e) {
		if(e.getPlayer().getGameMode().equals(GameMode.ADVENTURE)) e.setCancelled(true);
	}
	
	@EventHandler
	public void onPlayerTeleport(PlayerTeleportEvent e) {
		if(e.getPlayer().hasPermission("mcv.worldtp")) return;
		if(e.getCause().equals(TeleportCause.COMMAND) && (e.getTo().getWorld().equals(MCV.getPlugin().getServer().getWorld("world_mcv")) || mainApi.isInRegion(e.getTo(), "pvparena"))) {
			e.setCancelled(true);
			e.getPlayer().sendMessage("�3[MCV] �cTeleporteaza a fost anulata deoarece aceasta locatie nu face parte din lumea principala!");
		}
	}
	
	public void setupSpectateMCV() {
		mainApi = MCV.mainApi;
	}
	
}
