package me.the_all_nighta.mcv;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.WorldCreator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

public class MCV extends JavaPlugin implements Listener{
	
	
	ArrayList<String> allowedCmds = new ArrayList<String>();
	public static Plugin plugin;
	public static MainAPI mainApi = new MainAPI();
	public static me.the_all_nighta.mcv.jobs.Skills Skills = new me.the_all_nighta.mcv.jobs.Skills();
	public static me.the_all_nighta.mcv.jobs.farmer.Farmer Farmer = new me.the_all_nighta.mcv.jobs.farmer.Farmer();
	public static me.the_all_nighta.mcv.jobs.farmer.FarmerAPI FarmerAPI = new me.the_all_nighta.mcv.jobs.farmer.FarmerAPI();
	public static me.the_all_nighta.mcv.jobs.fisherman.Fisher Fisher = new me.the_all_nighta.mcv.jobs.fisherman.Fisher();
	public static me.the_all_nighta.mcv.jobs.fisherman.FisherAPI FisherAPI = new me.the_all_nighta.mcv.jobs.fisherman.FisherAPI();
	public static me.the_all_nighta.mcv.jobs.miner.Miner Miner = new me.the_all_nighta.mcv.jobs.miner.Miner();
	public static me.the_all_nighta.mcv.jobs.miner.MinerAPI MinerAPI = new me.the_all_nighta.mcv.jobs.miner.MinerAPI();
	public static me.the_all_nighta.mcv.jobs.woodcutter.Woodcutter Woodcutter = new me.the_all_nighta.mcv.jobs.woodcutter.Woodcutter();
	public static me.the_all_nighta.mcv.jobs.woodcutter.WoodcutterAPI WoodcutterAPI = new me.the_all_nighta.mcv.jobs.woodcutter.WoodcutterAPI();
	
	
	@Override
	public void onEnable()
	{
		plugin = this;
		Bukkit.getPluginManager().registerEvents(this, this);
		Database.connectToDB();
		getServer().createWorld(new WorldCreator("world_mcv"));
		Bukkit.getServer().getWorlds().add(Bukkit.getWorld("world_mcv"));
		setupSecondaryPlugins();
		allowedCmdsSetup();
	}
	
	@Override
	public void onDisable()
	{
		plugin = null;
		Database.disconnectFromDB();
	}
	
	public static Plugin getPlugin()
	{
		return plugin;
	}
	
	public static Server GetServer() {
		return plugin.getServer();
	}
	
	public static BukkitScheduler GetScheduler() {
		return plugin.getServer().getScheduler();
	}
	
	public static MainAPI getMainAPI() {
		return mainApi;
	}
	
	public void setupSecondaryPlugins()
	{
		mainApi.setupMainAPI();
		PluginManager pm = Bukkit.getServer().getPluginManager();
		pm.registerEvents(me.the_all_nighta.mcv.dice.Dice.getInstance(), this);
		pm.registerEvents(Farmer, this);
		pm.registerEvents(Fisher, this);
		pm.registerEvents(Miner, this);
		pm.registerEvents(Woodcutter, this);
		pm.registerEvents(me.the_all_nighta.mcv.general.FreeElytra.getInstance(), this);
		pm.registerEvents(me.the_all_nighta.mcv.general.PremiumFirework.getInstance(), this);
		pm.registerEvents(me.the_all_nighta.mcv.adminduty.SpectateMCV.getInstance(), this);
		pm.registerEvents(me.the_all_nighta.mcv.payday.PayDay.getInstance(), this);
		pm.registerEvents(me.the_all_nighta.mcv.general.StopProxy.getInstance(), this);
		pm.registerEvents(me.the_all_nighta.mcv.general.SpawnerDrop.getInstance(), this);
		getCommand("dice").setExecutor(me.the_all_nighta.mcv.dice.Dice.getInstance());
		getCommand("adice").setExecutor(me.the_all_nighta.mcv.dice.Dice.getInstance());
		getCommand("spawnviminer").setExecutor(Miner);
		getCommand("spawnvifarmer").setExecutor(Farmer);
		getCommand("spawnvifisher").setExecutor(Fisher);
		getCommand("spawnviwoodcutter").setExecutor(Woodcutter);
		getCommand("skills").setExecutor(Skills);
		getCommand("elytra").setExecutor(me.the_all_nighta.mcv.general.FreeElytra.getInstance());
		getCommand("boost").setExecutor(me.the_all_nighta.mcv.general.PremiumFirework.getInstance());
		getCommand("spectate").setExecutor(me.the_all_nighta.mcv.adminduty.SpectateMCV.getInstance());
		getCommand("stopspectate").setExecutor(me.the_all_nighta.mcv.adminduty.SpectateMCV.getInstance());
		getCommand("g").setExecutor(me.the_all_nighta.mcv.general.GlobalChat.getInstance());
		getCommand("a").setExecutor(me.the_all_nighta.mcv.adminduty.AdminChat.getInstance());
		getCommand("ore").setExecutor(me.the_all_nighta.mcv.payday.PayDay.getInstance());
		getCommand("sproxy").setExecutor(me.the_all_nighta.mcv.general.StopProxy.getInstance());
		Skills.loadSkillsPlugin();
		FarmerAPI.setupFarmer();
		FisherAPI.setupFisher();
		MinerAPI.setupMiner();
		WoodcutterAPI.setupWoodcutter();
		me.the_all_nighta.mcv.general.FreeElytra.getInstance().setupFreeElytra();
		me.the_all_nighta.mcv.general.PremiumFirework.getInstance().setupPremiumFirework();
		me.the_all_nighta.mcv.adminduty.SpectateMCV.getInstance().setupSpectateMCV();
		me.the_all_nighta.mcv.general.GlobalChat.getInstance().setupGlobalChat();
		me.the_all_nighta.mcv.payday.PayDay.getInstance().setupPayDay();
		me.the_all_nighta.mcv.general.WorldSave.getInstance().runLoop();
		me.the_all_nighta.mcv.general.StopProxy.getInstance().setupPlugin();
	}
	
	public void allowedCmdsSetup()
	{
		allowedCmds.add("/a"); allowedCmds.add("/g"); allowedCmds.add("/c"); allowedCmds.add("/money"); allowedCmds.add("/balance"); allowedCmds.add("/bal");
		allowedCmds.add("/stopspectate");
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if(cmd.getName().equalsIgnoreCase("teleportmcv"))
		{
			Player player = (Player) sender;
			if(!player.hasPermission("mcv.admin.teleportmcv")) return true;
			player.teleport(new Location(Bukkit.getWorld("world_mcv"), 0, 100, 0));
			return true;
		}
		else if(cmd.getName().equalsIgnoreCase("teleportmcv2"))
		{
			Player player = (Player) sender;
			if(!player.hasPermission("mcv.admin.teleportmcv")) return true;
			player.teleport(new Location(Bukkit.getWorld("world_bla"), 0, 100, 0));
			return true;
		}
		return false;
	}
	
	@EventHandler
	public void onPlayerPrecommand(PlayerCommandPreprocessEvent e)
	{
		if(e.getMessage().split(" ")[0].contains(":")) {
			e.setCancelled(true);
			e.getPlayer().sendMessage("�3[MCV] �cNu poti folosi �a':'�c in comenzi!");
			return;
		}
		if(e.getPlayer().getWorld().getName().equals("world_mcv") || mainApi.isInRegion(e.getPlayer(), "pvparena"))
		{
			String cmd = e.getMessage().split(" ")[0];
			if(e.getPlayer().hasPermission("mcv.owner")) return;
			if(!allowedCmds.contains(cmd.toLowerCase()))
			{
				e.setCancelled(true);
				e.getPlayer().sendMessage("�3[MCV] �cNu poti folosi aceasta comanda aici!");
			}
		}
	}
	
	
	
	@EventHandler
	public void onNetherTeleport(PlayerPortalEvent e)
	{
		if(e.isCancelled()) return;
		if(e.getPlayer() == null) return;
		if(mainApi.isInRegion(e.getPlayer(), "spawnportals")) 
		{
			Player p = e.getPlayer();
			e.setCancelled(true);
			p.setFlying(false); p.setAllowFlight(false);
			if(mainApi.isInRegion(p, "enterportalminer"))
			{
				Miner.addPlayerToMiner(p);
			}
			else if(mainApi.isInRegion(p, "enterportalwoodcutter"))
			{
				Woodcutter.addPlayerToWoodcutter(p);
			}
			else if(mainApi.isInRegion(p, "enterportalfarmer"))
			{
				Farmer.addPlayerToFarmer(p);
			}
			else if(mainApi.isInRegion(p, "enterportalfisher"))
			{
				Fisher.addPlayerToFisher(p);
			}
		}
		else if(e.getPlayer().getLocation().getWorld().getName().equals("world_mcv"))
		{
			Player p = e.getPlayer();
			e.setCancelled(true);
			if(mainApi.isInRegion(p, "exitportalminer"))
			{
				Miner.removePlayerFromMiner(p);
				spawnTeleport(p);
			}
			else if(mainApi.isInRegion(p, "exitportalwoodcutter"))
			{
				Woodcutter.removePlayerFromWoodcutter(p);
				spawnTeleport(p);
			}
			else if(mainApi.isInRegion(p, "exitportalfarmer"))
			{
				Farmer.removeFromFarmer(p);
				spawnTeleport(p);
			}
			else if(mainApi.isInRegion(p, "exitportalfisher"))
			{
				Fisher.removePlayerFromFisher(p);
				spawnTeleport(p);
			}
		}
		else if(mainApi.isInRegion(e.getPlayer(), "spawnpvpteleport"))
		{
			e.setCancelled(true);
			e.getPlayer().teleport(new Location(MCV.getPlugin().getServer().getWorld("world"), 6409.5, 151, 10236.5));
		}
		else if(mainApi.isInRegion(e.getPlayer(), "pvparena")) {
			e.setCancelled(true);
			spawnTeleport(e.getPlayer());
		}
	}
	
	public void spawnTeleport(Player player)
	{
		for(int i=0;i<2;i++) player.teleport(new Location(getServer().getWorld("world"), 6388.5, 144.2, 10422.5));
	}
	
	
}
