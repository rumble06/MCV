package me.the_all_nighta.mcv.inventory;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import org.bukkit.inventory.Inventory;

import me.the_all_nighta.mcv.Database;
import me.the_all_nighta.mcv.MCV;

public class InventoryDatabase {
	
	HashMap<String, Inventory> inventoryHolder = new HashMap<String, Inventory>();
	
	public Inventory getInventory(String inventoryName) {
		inventoryName = inventoryName.toLowerCase();
		if(inventoryHolder.containsKey(inventoryName)) return inventoryHolder.get(inventoryName);
		return null;
	}
	
	public void setInventory(String inventoryName, Inventory inventory) {
		inventoryName = inventoryName.toLowerCase();
		inventoryHolder.put(inventoryName, inventory);
		saveInventoryToDB(inventoryName);
	}
	
	public void loadInventory(String inventoryName, Inventory defaultInventory) {
		inventoryName = inventoryName.toLowerCase();
		try(Connection con = Database.getHikari().getConnection()) {
			PreparedStatement ps = con.prepareStatement("SELECT `base64` FROM `INVENTORIES` WHERE `inventory` = ?");
			ps.setString(1, inventoryName);
			ResultSet rs = ps.executeQuery();
			if(rs.next())
				inventoryHolder.put(inventoryName, InventoryBase64.fromBase64(rs.getString("base64")));
			else
				inventoryHolder.put(inventoryName, defaultInventory);
		} catch(SQLException | IOException e) {
			e.printStackTrace();
		}
	}
	
	public void unloadInventory(String inventoryName) {
		inventoryName = inventoryName.toLowerCase();
		if(!inventoryHolder.containsKey(inventoryName)) return;
		saveInventoryToDB(inventoryName);
		inventoryHolder.remove(inventoryName);
	}
	
	public void saveInventoryToDB(String inventoryName) {
		inventoryName = inventoryName.toLowerCase();
		if(!inventoryHolder.containsKey(inventoryName)) return;
		final String invName = inventoryName;
		MCV.getPlugin().getServer().getScheduler().runTaskAsynchronously(MCV.getPlugin(), new Runnable() {
			@Override
			public void run() {
				try(Connection con=Database.getHikari().getConnection()) {
					PreparedStatement ps = con.prepareStatement("INSERT INTO `INVENTORIES` (`inventory`, `base64`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `base64` = VALUES(`base64`)");
					ps.setString(1, invName);
					ps.setString(2, InventoryBase64.toBase64(inventoryHolder.get(invName)));
					ps.executeUpdate();
				} catch(SQLException e) {
					saveInventoryToDB(invName);
					e.printStackTrace();
				}
			}});
	}
	
	/*public void saveEverythingToDB() {
		for(String inventoryName : inventoryHolder.keySet())
			saveInventoryToDB(inventoryName);
	}*/
	
	/*public void saveLoop() {
		MCV.getPlugin().getServer().getScheduler().runTaskTimerAsynchronously(MCV.getPlugin(), new Runnable() {
			@Override
			public void run() {
				saveEverythingToDB();
			}
		}, 20L*60*5, 20L*60*5);
	}*/
	
}
