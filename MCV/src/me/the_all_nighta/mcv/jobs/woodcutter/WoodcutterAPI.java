package me.the_all_nighta.mcv.jobs.woodcutter;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;

import me.the_all_nighta.mcv.MCV;

public class WoodcutterAPI {
	
	private Location npcloc,tploc;
	private double defaultY=0;
	private String npcname;
	private ArrayList<String> joinedplayers = new ArrayList<String>();
	
	public void setupWoodcutter()
	{
		MCV.Woodcutter.mainApi = MCV.mainApi;
		MCV.Woodcutter.api = MCV.WoodcutterAPI;
		MCV.Woodcutter.skills = MCV.Skills;
		MCV.Woodcutter.skills.setupJob("Woodcutter");
		createConfig();
		loadConfig();
		me.the_all_nighta.mcv.MCV.Woodcutter.checkPlatform();
	}
	
	private void createConfig()
	{
		MCV.getPlugin().getConfig().addDefault("jobs.woodcutter.defaultY", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.woodcutter.teleport.x", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.woodcutter.teleport.y", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.woodcutter.teleport.z", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.woodcutter.teleport.yaw", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.woodcutter.teleport.pitch", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.woodcutter.npc.name", "Padurar");
		MCV.getPlugin().getConfig().addDefault("jobs.woodcutter.npc.x", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.woodcutter.npc.y", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.woodcutter.npc.z", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.woodcutter.npc.yaw", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.woodcutter.npc.pitch", 0);
		MCV.getPlugin().getConfig().options().copyDefaults(true);
		MCV.getPlugin().saveConfig();
	}
	
	private void loadConfig()
	{
		defaultY = MCV.getPlugin().getConfig().getDouble("jobs.woodcutter.defaultY");
		tploc = new Location(MCV.getPlugin().getServer().getWorld("world_mcv"), MCV.getPlugin().getConfig().getDouble("jobs.woodcutter.teleport.x"), MCV.getPlugin().getConfig().getDouble("jobs.woodcutter.teleport.y"),
				MCV.getPlugin().getConfig().getDouble("jobs.woodcutter.teleport.z"), (float) MCV.getPlugin().getConfig().getDouble("jobs.woodcutter.teleport.yaw"), (float) MCV.getPlugin().getConfig().getDouble("jobs.woodcutter.teleport.pitch"));
		npcloc = new Location(MCV.getPlugin().getServer().getWorld("world_mcv"), MCV.getPlugin().getConfig().getDouble("jobs.woodcutter.npc.x"), MCV.getPlugin().getConfig().getDouble("jobs.woodcutter.npc.y"),
				MCV.getPlugin().getConfig().getDouble("jobs.woodcutter.npc.z"), (float) MCV.getPlugin().getConfig().getDouble("jobs.woodcutter.npc.yaw"), (float) MCV.getPlugin().getConfig().getDouble("jobs.woodcutter.npc.pitch"));
		npcname = MCV.getPlugin().getConfig().getString("jobs.woodcutter.npc.name");
	}
	
	public void addPlayer(Player player)
	{
		joinedplayers.add(player.getName().toLowerCase());
	}
	
	public void removePlayer(Player player)
	{
		joinedplayers.remove(player.getName().toLowerCase());
	}
	
	public boolean isJoined(Player player)
	{
		if(joinedplayers.contains(player.getName().toLowerCase())) return true;
		return false;
	}
	
	public Location getTPloc()
	{
		return tploc;
	}
	
	public String getNPCname()
	{
		return npcname;
	}
	
	public void createNPC()
	{
		Villager npc = (Villager) MCV.getPlugin().getServer().getWorld("world_mcv").spawnEntity(npcloc, EntityType.VILLAGER);
		npc.setCanPickupItems(false); npc.setInvulnerable(true); npc.setAI(false); npc.setCustomNameVisible(true); npc.setProfession(Villager.Profession.BUTCHER); npc.setCustomName(npcname);
	}
	
	public double getDefaultY()
	{
		return defaultY;
	}
	
	public ArrayList<String> getAllJoined()
	{
		return joinedplayers;
	}
	
}
