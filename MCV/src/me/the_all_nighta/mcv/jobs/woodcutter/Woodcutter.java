package me.the_all_nighta.mcv.jobs.woodcutter;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.the_all_nighta.mcv.MCV;
import me.the_all_nighta.mcv.MainAPI;
import me.the_all_nighta.mcv.jobs.Skills;

public class Woodcutter implements CommandExecutor, Listener{
	
	MainAPI mainApi;
	WoodcutterAPI api;
	Skills skills;
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("spawnviwoodcutter"))
		{
			if(!sender.hasPermission("mcv.jobs.admin")) return true;
			api.createNPC();
			return true;
		}
		
		return false;
	}
	
	@EventHandler
	public void onPlayerEntityInteract(PlayerInteractEntityEvent e)
	{
		if(e.getHand().equals(EquipmentSlot.HAND) && api.isJoined(e.getPlayer()) && e.getRightClicked().getName().equals(api.getNPCname()) && mainApi.isInRegion(e.getPlayer(), "jobtaietor"))
		{
			e.setCancelled(true);
			if(mainApi.getItemAmount(e.getPlayer(), Material.STONE_AXE) > 0) 
			{
				e.getPlayer().sendMessage(String.format("�3%s: �cDu-te si sparge lemne iar apoi urca-te cu ele pe platforma!", api.getNPCname()));
			}
			else
			{
				e.getPlayer().sendMessage("�3"+api.getNPCname()+"�f: Cauti un loc de munca?\n"
						+ e.getPlayer().getDisplayName()+"�f: Am venit sa castig niste bani!\n"
						+ "�3"+api.getNPCname()+"�f: Pune mana pe un �atopor �fsi apuca-te de taiat.\n"
						+ "�3"+api.getNPCname()+"�f: Avem nevoie de busteni pentru cherestea, avem multe comenzi de livrat!\n"
						+ "\n"
						+ "�3"+api.getNPCname()+"�f: Iti voi da un topor cu care va trebui sa dobori copacii de la baza si sa aduci lemnul pe �aplatforma �fpentru a-l incarca in dirijabil.");
				ItemStack is = new ItemStack(Material.STONE_AXE, 1); ItemMeta ism = is.getItemMeta(); ism.setUnbreakable(true); is.setItemMeta(ism);
				e.getPlayer().getInventory().addItem(is);
			}
		}
	}
	
	@EventHandler
	public void playerBlockBreak(BlockBreakEvent e)
	{
		if(api.isJoined(e.getPlayer()) && (e.getBlock().getType().equals(Material.LOG) || e.getBlock().getType().equals(Material.LEAVES)) && mainApi.isInRegion(e.getBlock(), "woodCutterpamant"))
		{
			if(e.getBlock().getType().equals(Material.LOG))
			{
				e.setCancelled(true);
				double y=api.getDefaultY(),x=e.getBlock().getX(),z=e.getBlock().getZ(),currentY=e.getBlock().getY();
				if(MCV.getPlugin().getServer().getWorld("world_mcv").getBlockAt(new Location(MCV.getPlugin().getServer().getWorld("world_mcv"),x,currentY+1,z)).getType().equals(Material.LOG))
				{
					Location loc = new Location(MCV.getPlugin().getServer().getWorld("world_mcv"), x, y, z);
					mainApi.pasteSchematic("aer.schematic", loc, true);
					mainApi.pasteSchematic("copaccazut.schematic", loc, false);
					mainApi.pasteSchematicDelay("copac", x, y, z-4, 40);
					mainApi.pasteSchematicDelay("air2", x, y, z, 40, true);
				}
				else
				{
					e.getBlock().setType(Material.AIR);
					e.getPlayer().getInventory().addItem(new ItemStack(Material.LOG, 1));
				}
			}
			else if(e.getBlock().getType().equals(Material.LEAVES))
			{
				e.setCancelled(true);
				e.getBlock().setType(Material.AIR);
			}
		}
	}
	
	@EventHandler
	public void onPlayerCraft(CraftItemEvent e)
	{
		if(api.isJoined((Player)e.getWhoClicked())) e.setCancelled(true);
	}
	
	public void addPlayerToWoodcutter(Player player)
	{
		if(!player.getGameMode().equals(GameMode.SURVIVAL))
		{
			player.sendMessage("�3[Jobs] �cTrebuie sa fi in gamemode-ul �2survival�c pentru a intra la un job!");
			return;
		}
		for(ItemStack it : player.getInventory().getContents())
		{
			if(it!=null)
			{
				player.sendMessage("�3[Jobs] �cTrebuie sa ai inventarul gol cand intrii la un job!");
				return;
			}
		}
		api.addPlayer(player);
		player.teleport(api.getTPloc());
	}
	
	public void removePlayerFromWoodcutter(Player player)
	{
		if(!api.isJoined(player)) return;
		api.removePlayer(player);
		player.getInventory().clear();
	}
	
	@EventHandler
	public void onPlayerKick(PlayerKickEvent e)
	{
		removePlayerFromWoodcutter(e.getPlayer());
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e)
	{
		removePlayerFromWoodcutter(e.getPlayer());
	}
	
	public void checkPlatform()
	{
		MCV.getPlugin().getServer().getScheduler().runTaskTimer(MCV.getPlugin(), new Runnable() {
			public void run()
			{
				for(String splayer : api.getAllJoined())
				{
					Player player = MCV.getPlugin().getServer().getPlayerExact(splayer);
					if(mainApi.isInRegion(player, "WoodCutterPlatforma") && player.getInventory().contains(Material.LOG))
					{
						double money = 3.2;
						money += 0.05*(skills.getSkill(player, "woodcutter") - 1)*money;
						player.getInventory().removeItem(new ItemStack(Material.LOG, 1));
						player.sendMessage("�3[MCV] �fAi primit �a$"+money+"�f in schimbul a �a1 lemn�f!");
						mainApi.getEconomy().depositPlayer(player, 3.2);
						skills.addLivrari(player, "woodcutter", 1);
					}
				}
			}
		}, 10L, 10L);
	}

}
