package me.the_all_nighta.mcv.jobs.miner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.ItemStack;

import me.the_all_nighta.mcv.MCV;
import me.the_all_nighta.mcv.MainAPI;

public class MinerAPI {
	
	private Location npcloc,tploc;
	private HashMap<String, Integer> missionStages = new HashMap<String, Integer>();
	private String npcname;
	MainAPI mainApi;
	ItemStack packet0, packet1, packet2;
	
	public void setupMiner()
	{
		MCV.Miner.mainApi = MCV.mainApi;
		mainApi = MCV.mainApi;
		MCV.Miner.api = MCV.MinerAPI;
		MCV.Miner.skills = MCV.Skills;
		MCV.Miner.skills.setupJob("miner");
		createConfig();
		loadConfig();
		initializePackets();
		me.the_all_nighta.mcv.MCV.Miner.checkLoop();
	}
	
	private void createConfig()
	{
		MCV.getPlugin().getConfig().addDefault("jobs.miner.npc.name", "Miner-Sef");
		MCV.getPlugin().getConfig().addDefault("jobs.miner.npc.x", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.miner.npc.y", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.miner.npc.z", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.miner.npc.yaw", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.miner.npc.pitch", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.miner.teleport.x", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.miner.teleport.y", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.miner.teleport.z", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.miner.teleport.yaw", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.miner.teleport.pitch", 0);
		MCV.getPlugin().getConfig().options().copyDefaults(true);
		MCV.getPlugin().saveConfig();
	}
	
	private void loadConfig()
	{
		npcloc = new Location(MCV.getPlugin().getServer().getWorld("world_mcv"), MCV.getPlugin().getConfig().getDouble("jobs.miner.npc.x"), MCV.getPlugin().getConfig().getDouble("jobs.miner.npc.y"),
				MCV.getPlugin().getConfig().getDouble("jobs.miner.npc.z"), (float) MCV.getPlugin().getConfig().getDouble("jobs.miner.npc.yaw"), (float) MCV.getPlugin().getConfig().getDouble("jobs.miner.npc.pitch"));
		tploc =  new Location(MCV.getPlugin().getServer().getWorld("world_mcv"), MCV.getPlugin().getConfig().getDouble("jobs.miner.teleport.x"), MCV.getPlugin().getConfig().getDouble("jobs.miner.teleport.y"),
				MCV.getPlugin().getConfig().getDouble("jobs.miner.teleport.z"), (float) MCV.getPlugin().getConfig().getDouble("jobs.miner.teleport.yaw"), (float) MCV.getPlugin().getConfig().getDouble("jobs.miner.teleport.pitch"));
		npcname = MCV.getPlugin().getConfig().getString("jobs.miner.npc.name");
	}
	
	private void initializePackets()
	{
		String message = "Livreaza acest pachet in vagon pepntru a primi recompensa!";
		packet0 = mainApi.setItemTitle(mainApi.setItemLores(new ItemStack(Material.ENDER_CHEST, 1), Arrays.asList(message, 
				"�bContine:", "�f-> 30 Gold Ingot", "�f-> 18 Iron Ingot", "�f-> 10 Coal", "�f-> 20 Gravel")), "�ePachet 1");
		packet1 = mainApi.setItemTitle(mainApi.setItemLores(new ItemStack(Material.ENDER_CHEST, 1), Arrays.asList(message, 
				"�bContine:", "�f-> 30 Redstone", "�f-> 25 Sand", "�f-> 5 Diamond", "�f-> 40 End Stone")), "�ePachet 2");
		packet2 = mainApi.setItemTitle(mainApi.setItemLores(new ItemStack(Material.ENDER_CHEST, 1), Arrays.asList(message,
				"�bContine:", "�f-> 25 Quartz", "�f-> 30 Soulsand", "�f-> 30 Lapis", "�f-> 5 Emerald", "�f-> 15 Mossy Stone", "�f-> 15 Cobblestone")), "�ePachet 3");
	}
	
	public ItemStack getPacket(int packetnr)
	{
		if(packetnr == 0) return packet0;
		if(packetnr == 1) return packet1;
		if(packetnr == 2) return packet2;
		else return new ItemStack(Material.AIR);
	}
	
	public Location getTPloc()
	{
		return tploc;
	}
	
	public void createNPC()
	{
		Villager npc = (Villager) MCV.getPlugin().getServer().getWorld("world_mcv").spawnEntity(npcloc, EntityType.VILLAGER);
		npc.setCanPickupItems(false); npc.setInvulnerable(true); npc.setAI(false); npc.setCustomNameVisible(true); npc.setProfession(Villager.Profession.BUTCHER); npc.setCustomName(npcname);
	}
	
	public void updateStage(Player player, int stage)
	{
		String pl = player.getName().toLowerCase();
		if(stage==-1) missionStages.remove(pl);
		else missionStages.put(pl, stage);
	}
	
	public int getStage(Player player)
	{
		if(missionStages.containsKey(player.getName().toLowerCase())) return missionStages.get(player.getName().toLowerCase());
		return -1;
	}
	
	public ArrayList<String> getAllJoined()
	{
		ArrayList<String> joined = new ArrayList<String>();
		joined.addAll(missionStages.keySet());
		return joined;
	}
	
	public String getNPCname()
	{
		return npcname;
	}
	
	public String getFullMessage(Player pl)
	{
		String message1 = "Ai deja ";
		String message2 = "Mai ai nevoie de: ";
		ArrayList<String> alreadyHave = new ArrayList<String>();
		ArrayList<String> notHave = new ArrayList<String>();
		if(getStage(pl) == 1)
		{
			if(mainApi.getItemAmount(pl, Material.GOLD_INGOT) >= 30) alreadyHave.add("30/30 Gold");
			else notHave.add(String.format("%s/30 Gold", mainApi.getItemAmount(pl, Material.GOLD_INGOT)));
			if(mainApi.getItemAmount(pl, Material.IRON_INGOT) >= 18) alreadyHave.add("18/18 Iron");
			else notHave.add(String.format("%s/18 Iron", mainApi.getItemAmount(pl, Material.IRON_INGOT)));
			if(mainApi.getItemAmount(pl, Material.COAL) >= 10) alreadyHave.add("10/10 Coal");
			else notHave.add(String.format("%s/10 Coal", mainApi.getItemAmount(pl, Material.COAL)));
			if(mainApi.getItemAmount(pl, Material.GRAVEL) >= 20) alreadyHave.add("20/20 Gravel");
			else notHave.add(String.format("%s/20 Gravel", mainApi.getItemAmount(pl, Material.GRAVEL)));
		}
		else if(getStage(pl) == 3)
		{
			if(mainApi.getItemAmount(pl, Material.REDSTONE) >= 30) alreadyHave.add("30/30 Redstone");
			else notHave.add(String.format("%s/30 Redstone", mainApi.getItemAmount(pl, Material.REDSTONE)));
			if(mainApi.getItemAmount(pl, Material.SAND) >= 25) alreadyHave.add("25/25 Sand");
			else notHave.add(String.format("%s/25 Sand", mainApi.getItemAmount(pl, Material.SAND)));
			if(mainApi.getItemAmount(pl, Material.DIAMOND) >= 5) alreadyHave.add("5/5 Diamond");
			else notHave.add(String.format("%s/5 Diamond", mainApi.getItemAmount(pl, Material.DIAMOND)));
			if(mainApi.getItemAmount(pl, Material.ENDER_STONE) >= 40) alreadyHave.add("40/40 End Stone");
			else notHave.add(String.format("%s/40 End Stone", mainApi.getItemAmount(pl, Material.ENDER_STONE)));
		}
		else if(getStage(pl) == 5)
		{
			if(mainApi.getItemAmount(pl, Material.QUARTZ) >= 25) alreadyHave.add("25/25 Quartz");
			else notHave.add(String.format("%s/40 Quartz", mainApi.getItemAmount(pl, Material.QUARTZ)));
			if(mainApi.getItemAmount(pl, Material.SOUL_SAND) >= 30) alreadyHave.add("30/30 Soul Sand");
			else notHave.add(String.format("%s/30 Soul Sand", mainApi.getItemAmount(pl, Material.SOUL_SAND)));
			if(mainApi.getItemAmount(pl, Material.INK_SACK, 4) >= 30) alreadyHave.add("30/30 Lapis");
			else notHave.add(String.format("%s/30 Lapis", mainApi.getItemAmount(pl, Material.INK_SACK, 4)));
			if(mainApi.getItemAmount(pl, Material.EMERALD) >= 5) alreadyHave.add("5/5 Emerald");
			else notHave.add(String.format("%s/5 Emerald", mainApi.getItemAmount(pl, Material.EMERALD)));
			if(mainApi.getItemAmount(pl, Material.MOSSY_COBBLESTONE) >= 15) alreadyHave.add("15/15 Moss Stone");
			else notHave.add(String.format("%s/15 Moss Stone", mainApi.getItemAmount(pl, Material.MOSSY_COBBLESTONE)));
			if(mainApi.getItemAmount(pl, Material.COBBLESTONE) >= 15) alreadyHave.add("15/15 Cobblestone");
			else notHave.add(String.format("%s/15 Cobblestone", mainApi.getItemAmount(pl, Material.COBBLESTONE)));
		}
		for(int i=0;i<alreadyHave.size();i++)
		{
			if(i == alreadyHave.size() - 1)
			{
				message1+=alreadyHave.get(i)+". ";
			}
			else
			{
				message1+=alreadyHave.get(i)+", ";
			}
		}
		for(int i=0;i<notHave.size();i++)
		{
			if(i == notHave.size() - 1)
			{
				message2+=notHave.get(i)+".";
			}
			else
			{
				message2+=notHave.get(i)+", ";
			}
		}
		if(notHave.size() == 0) return message1+"Intoarce-te la [NPC]Miner-Sef cu materialele!";
		return message1+message2;
	}
	
}
