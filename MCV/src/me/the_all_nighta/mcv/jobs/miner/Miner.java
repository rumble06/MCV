package me.the_all_nighta.mcv.jobs.miner;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.the_all_nighta.mcv.MCV;
import me.the_all_nighta.mcv.MainAPI;
import me.the_all_nighta.mcv.jobs.Skills;

public class Miner implements Listener, CommandExecutor {
	
	MainAPI mainApi;
	MinerAPI api;
	Skills skills;
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("spawnviminer"))
		{
			if(!sender.hasPermission("mcv.jobs.admin")) return true;
			api.createNPC();
			return true;
		}
		
		return false;
	}
	
	public void addPlayerToMiner(Player player)
	{
		if(player.getGameMode()!=GameMode.SURVIVAL)
		{
			player.sendMessage("�3[Jobs] �cTrebuie sa fi in gamemode-ul �2survival�c pentru a intra la un job!");
			return;
		}
		for(ItemStack it : player.getInventory().getContents())
		{
			if(it!=null)
			{
				player.sendMessage("�3[Jobs] �cTrebuie sa ai inventarul gol cand intrii la un job!");
				return;
			}
		}
		player.teleport(api.getTPloc());
		player.sendMessage(String.format("�3[Jobs] �fAi intrat la job-ul �2miner�f, du-te si intreaba pe �3%s�f daca are de munca pentru tine!", api.getNPCname()));
		api.updateStage(player, 0);
	}
	
	public void removePlayerFromMiner(Player player)
	{
		if(api.getStage(player) == -1) return;
		api.updateStage(player, -1);
		player.getInventory().clear();
	}
	
	@EventHandler
	public void npcInteract(PlayerInteractEntityEvent e)
	{
		if(e.getHand().equals(EquipmentSlot.HAND) && e.getPlayer().getWorld().equals(MCV.getPlugin().getServer().getWorld("world_mcv")) 
				&& mainApi.isInRegion(e.getPlayer(), "jobminer") && e.getRightClicked().getName().equals(api.getNPCname()))
		{
			e.setCancelled(true);
			if(e.getPlayer().getInventory().contains(api.getPacket(0)) || e.getPlayer().getInventory().contains(api.getPacket(1)) || e.getPlayer().getInventory().contains(api.getPacket(2))) {
				e.getPlayer().sendMessage(String.format("�3%s�f: Ai deja pachetul necesar, du-te pe �2platforma�f cu el pentru a-ti primi recompensa!", api.getNPCname()));
				return;
			}
			if(api.getStage(e.getPlayer()) == 0)
			{
				api.updateStage(e.getPlayer(), 1);
				ItemStack is = new ItemStack(Material.IRON_PICKAXE); ItemStack is2 = new ItemStack(Material.IRON_SPADE);
				ItemMeta ism = is.getItemMeta(); ItemMeta ism2 = is2.getItemMeta(); ism.setUnbreakable(true); ism2.setUnbreakable(true);
				is.setItemMeta(ism); is2.setItemMeta(ism2);
				e.getPlayer().getInventory().addItem(is); e.getPlayer().getInventory().addItem(is2);
				e.getPlayer().sendMessage(String.format("�3%s�f: Ai venit sa castigi niste bani?\nIa acest tarnacop si adu-mi urmatoarele materiale:�2\nx30 Gold\nx18 Iron\nx10 Coal\nx20 Gravel", api.getNPCname()));
				return;
			}
			else if(api.getStage(e.getPlayer()) == 1)
			{
				if(mainApi.getItemAmount(e.getPlayer(), Material.GOLD_INGOT) < 30 || mainApi.getItemAmount(e.getPlayer(), Material.IRON_INGOT) < 18
						|| mainApi.getItemAmount(e.getPlayer(), Material.COAL) < 10 || mainApi.getItemAmount(e.getPlayer(), Material.GRAVEL) < 20)
				{
					e.getPlayer().sendMessage(String.format("�3%s�f: Ai strans �2%s�f/�230 Gold�f, �2%s�f/�218 Iron�f, �2%s�f/�210 Coal�f, �2%s�f/�220 Gravel�f, intoarce-te cand ai toate materialele!", 
							api.getNPCname(), mainApi.getItemAmount(e.getPlayer(), Material.GOLD_INGOT), mainApi.getItemAmount(e.getPlayer(), Material.IRON_INGOT), mainApi.getItemAmount(e.getPlayer(), Material.COAL), 
							mainApi.getItemAmount(e.getPlayer(), Material.GRAVEL)));
					return;
				}
				mainApi.removeItemAmount(e.getPlayer(), Material.GOLD_INGOT, 30);
				mainApi.removeItemAmount(e.getPlayer(), Material.IRON_INGOT, 18);
				mainApi.removeItemAmount(e.getPlayer(), Material.COAL, 10);
				mainApi.removeItemAmount(e.getPlayer(), Material.GRAVEL, 20);
				e.getPlayer().getInventory().addItem(api.getPacket(0));
				e.getPlayer().sendMessage(String.format("�3%s�f: Du acest pachet in vagon pentru a-ti primi recompensa!", api.getNPCname()));
				return;
			}
			else if(api.getStage(e.getPlayer()) == 2)
			{
				api.updateStage(e.getPlayer(), 3);
				e.getPlayer().sendMessage(String.format("�3%s�f: Adu-mi din pestera urmatoarele materiale:�2\nx30 Redstone\nx25 Sand\nx5 Diamond\nx40 End Stone", api.getNPCname()));
				return;
			}
			else if(api.getStage(e.getPlayer()) == 3)
			{
				if(mainApi.getItemAmount(e.getPlayer(), Material.REDSTONE) < 30 || mainApi.getItemAmount(e.getPlayer(), Material.SAND) < 25
						|| mainApi.getItemAmount(e.getPlayer(), Material.DIAMOND) < 5 || mainApi.getItemAmount(e.getPlayer(), Material.ENDER_STONE) < 40)
				{
					e.getPlayer().sendMessage(String.format("�3%s�f: Ai strans �2%s�f/�230 Redstone�f, �2%s�f/�225 Sand�f, �2%s�f/�25 Diamond�f, �2%s�f/�240 End Stone�f, intoarce-te cand ai toate materialele!", 
							api.getNPCname(), mainApi.getItemAmount(e.getPlayer(), Material.REDSTONE), mainApi.getItemAmount(e.getPlayer(), Material.SAND), mainApi.getItemAmount(e.getPlayer(), Material.DIAMOND), 
							mainApi.getItemAmount(e.getPlayer(), Material.ENDER_STONE)));
					return;
				}
				mainApi.removeItemAmount(e.getPlayer(), Material.REDSTONE, 30);
				mainApi.removeItemAmount(e.getPlayer(), Material.SAND, 25);
				mainApi.removeItemAmount(e.getPlayer(), Material.DIAMOND, 5);
				mainApi.removeItemAmount(e.getPlayer(), Material.ENDER_STONE, 40);
				e.getPlayer().getInventory().addItem(api.getPacket(1));
				e.getPlayer().sendMessage(String.format("�3%s�f: Du acest pachet in vagon pentru a-ti primi recompensa!", api.getNPCname()));
				return;
			}
			else if(api.getStage(e.getPlayer()) == 4)
			{
				api.updateStage(e.getPlayer(), 5);
				e.getPlayer().sendMessage(String.format("�3%s�f: Mai am o treaba pentru tine, adu-mi urmatoarele materiale:�2\nx25 Quartz\nx30 Soulsand\nx30 Lapis\nx5 Emerald\nx15 Moss Stone\nx15 Cobblestone", api.getNPCname()));
				return;
			}
			else if(api.getStage(e.getPlayer()) == 5)
			{
				if(mainApi.getItemAmount(e.getPlayer(), Material.QUARTZ) < 25 || mainApi.getItemAmount(e.getPlayer(), Material.SOUL_SAND) < 30
						|| mainApi.getItemAmount(e.getPlayer(), Material.INK_SACK, 4) < 30 || mainApi.getItemAmount(e.getPlayer(), Material.EMERALD) < 5
						|| mainApi.getItemAmount(e.getPlayer(), Material.MOSSY_COBBLESTONE) < 15 || mainApi.getItemAmount(e.getPlayer(), Material.COBBLESTONE) < 15)
				{
					e.getPlayer().sendMessage(String.format("�3%s�f: Ai strans �2%s�f/�225 Quartz�f, �2%s�f/�230 Soul Sand�f, �2%s�f/�230 Lapis�f, �2%s�f/�25 Emerald�f, �2%s�f/�215 Moss Stone�f, �2%s�f/�215 Cobblestone�f, intoarce-te cand ai toate materialele!", 
							api.getNPCname(), mainApi.getItemAmount(e.getPlayer(), Material.QUARTZ), mainApi.getItemAmount(e.getPlayer(), Material.SOUL_SAND), mainApi.getItemAmount(e.getPlayer(), Material.INK_SACK, 4), 
							mainApi.getItemAmount(e.getPlayer(), Material.EMERALD), mainApi.getItemAmount(e.getPlayer(), Material.MOSSY_COBBLESTONE), mainApi.getItemAmount(e.getPlayer(), Material.COBBLESTONE)));
					return;
				}
				mainApi.removeItemAmount(e.getPlayer(), Material.QUARTZ, 25);
				mainApi.removeItemAmount(e.getPlayer(), Material.SOUL_SAND, 30);
				mainApi.removeItemAmount(e.getPlayer(), Material.INK_SACK, 30, 4);
				mainApi.removeItemAmount(e.getPlayer(), Material.EMERALD, 5);
				mainApi.removeItemAmount(e.getPlayer(), Material.MOSSY_COBBLESTONE, 15);
				mainApi.removeItemAmount(e.getPlayer(), Material.COBBLESTONE, 15);
				e.getPlayer().getInventory().addItem(api.getPacket(2));
				e.getPlayer().sendMessage(String.format("�3%s�f: Du acest pachet in vagon pentru a-ti primi recompensa!", api.getNPCname()));
				return;
			}
		}
	}
	
	@EventHandler
	public void playerBlockBreakEvent(BlockBreakEvent e)
	{
		if(api.getStage(e.getPlayer()) == -1) return;
		if(mainApi.isInRegion(e.getPlayer(), "jobminer"))
		{
			if(e.getPlayer().getGameMode().equals(GameMode.SURVIVAL))
				e.setCancelled(true);
			if(api.getStage(e.getPlayer()) == 1)
			{
				if(e.getBlock().getType().equals(Material.GOLD_ORE) && mainApi.isInRegion(e.getBlock(), "jobminergold"))
				{
					if(mainApi.getItemAmount(e.getPlayer(), Material.GOLD_INGOT) >= 30)
						e.getPlayer().sendMessage(api.getFullMessage(e.getPlayer()));
					else
					{
						e.getBlock().setType(Material.AIR);
						e.getPlayer().getInventory().addItem(new ItemStack(Material.GOLD_INGOT, 1));
						mainApi.pasteSchematicDelay("gold", 52, 36, -301, 60);
					}
				}
				else if(e.getBlock().getType().equals(Material.IRON_ORE) && mainApi.isInRegion(e.getBlock(), "jobmineriron"))
				{
					if(mainApi.getItemAmount(e.getPlayer(), Material.IRON_INGOT) >= 18)
						e.getPlayer().sendMessage(api.getFullMessage(e.getPlayer()));
					else
					{
						e.getBlock().setType(Material.AIR);
						e.getPlayer().getInventory().addItem(new ItemStack(Material.IRON_INGOT, 1));
						mainApi.pasteSchematicDelay("iron", 55, 36, -290, 60);
					}
				}
				else if(e.getBlock().getType().equals(Material.COAL_ORE) && mainApi.isInRegion(e.getBlock(), "jobminercoal"))
				{
					if(mainApi.getItemAmount(e.getPlayer(), Material.COAL) >= 10)
						e.getPlayer().sendMessage(api.getFullMessage(e.getPlayer()));
					else
					{
						e.getBlock().setType(Material.AIR);
						e.getPlayer().getInventory().addItem(new ItemStack(Material.COAL, 1));
						mainApi.pasteSchematicDelay("coal", 58, 36, -284, 60);
					}
				}
				else if(e.getBlock().getType().equals(Material.GRAVEL) && mainApi.isInRegion(e.getBlock(), "jobminergravel"))
				{
					if(mainApi.getItemAmount(e.getPlayer(), Material.GRAVEL) >= 20)
						e.getPlayer().sendMessage(api.getFullMessage(e.getPlayer()));
					else
					{
						e.getBlock().setType(Material.AIR);
						e.getPlayer().getInventory().addItem(new ItemStack(Material.GRAVEL, 1));
						mainApi.pasteSchematicDelay("gravel", 67, 24, -382, 60);
					}
				}
			}
			else if(api.getStage(e.getPlayer()) == 3)
			{
				if((e.getBlock().getType().equals(Material.REDSTONE_ORE) || e.getBlock().getType().equals(Material.GLOWING_REDSTONE_ORE)) && mainApi.isInRegion(e.getBlock(), "jobminerredstone"))
				{
					if(mainApi.getItemAmount(e.getPlayer(), Material.REDSTONE) >= 30)
						e.getPlayer().sendMessage(api.getFullMessage(e.getPlayer()));
					else
					{
						e.getBlock().setType(Material.AIR);
						e.getPlayer().getInventory().addItem(new ItemStack(Material.REDSTONE, 1));
						mainApi.pasteSchematicDelay("redstone", 65, 36, -290, 60);
					}
				}
				else if(e.getBlock().getType().equals(Material.SAND) && mainApi.isInRegion(e.getBlock(), "jobminersand"))
				{
					if(mainApi.getItemAmount(e.getPlayer(), Material.SAND) >= 25)
						e.getPlayer().sendMessage(api.getFullMessage(e.getPlayer()));
					else
					{
						e.getBlock().setType(Material.AIR);
						e.getPlayer().getInventory().addItem(new ItemStack(Material.SAND, 1));
						mainApi.pasteSchematicDelay("sand", 55, 24, -382, 60);
					}
				}
				else if(e.getBlock().getType().equals(Material.DIAMOND_ORE) && (mainApi.isInRegion(e.getBlock(), "jobminerdiamond1") || mainApi.isInRegion(e.getBlock(), "jobminerdiamond2")
						|| mainApi.isInRegion(e.getBlock(), "jobminerdiamond3")))
				{
					if(mainApi.getItemAmount(e.getPlayer(), Material.DIAMOND) >= 5)
						e.getPlayer().sendMessage(api.getFullMessage(e.getPlayer()));
					else
					{
						e.getBlock().setType(Material.AIR);
						e.getPlayer().getInventory().addItem(new ItemStack(Material.DIAMOND, 1));
						if(mainApi.isInRegion(e.getBlock(), "jobminerdiamond1")) mainApi.pasteSchematicDelay("diamond", 158, 36, -356, 60);
						else if(mainApi.isInRegion(e.getBlock(), "jobminerdiamond2")) mainApi.pasteSchematicDelay("diamond", 161, 36, -366, 60);
						else if(mainApi.isInRegion(e.getBlock(), "jobminerdiamond3")) mainApi.pasteSchematicDelay("diamond", 179, 36, -352, 60);
					}
				}
				else if(e.getBlock().getType().equals(Material.ENDER_STONE) && mainApi.isInRegion(e.getBlock(), "jobminerendstone"))
				{
					if(mainApi.getItemAmount(e.getPlayer(), Material.ENDER_STONE) >= 40)
						e.getPlayer().sendMessage(api.getFullMessage(e.getPlayer()));
					else
					{
						e.getBlock().setType(Material.AIR);
						e.getPlayer().getInventory().addItem(new ItemStack(Material.ENDER_STONE, 1));
						mainApi.pasteSchematicDelay("endstone", 65, 36, -309, 90);
					}
				}
			}
			else if(api.getStage(e.getPlayer()) == 5)
			{
				if(e.getBlock().getType().equals(Material.QUARTZ_ORE) && mainApi.isInRegion(e.getBlock(), "jobminerquartz"))
				{
					if(mainApi.getItemAmount(e.getPlayer(), Material.QUARTZ) >= 25)
						e.getPlayer().sendMessage(api.getFullMessage(e.getPlayer()));
					else
					{
						e.getBlock().setType(Material.AIR);
						e.getPlayer().getInventory().addItem(new ItemStack(Material.QUARTZ, 1));
						mainApi.pasteSchematicDelay("quartz", 68, 36, -284, 60);
					}
				}
				else if(e.getBlock().getType().equals(Material.SOUL_SAND) && mainApi.isInRegion(e.getBlock(), "jobminersoulsand"))
				{
					if(mainApi.getItemAmount(e.getPlayer(), Material.SOUL_SAND) >= 30)
						e.getPlayer().sendMessage(api.getFullMessage(e.getPlayer()));
					else
					{
						e.getBlock().setType(Material.AIR);
						e.getPlayer().getInventory().addItem(new ItemStack(Material.SOUL_SAND, 1));
						mainApi.pasteSchematicDelay("soulsand", 73, 24, -394, 60);
					}
				}
				else if(e.getBlock().getType().equals(Material.LAPIS_ORE) && mainApi.isInRegion(e.getBlock(), "jobminerlapis"))
				{
					if(mainApi.getItemAmount(e.getPlayer(), Material.INK_SACK, 4) >= 30)
						e.getPlayer().sendMessage(api.getFullMessage(e.getPlayer()));
					else
					{
						e.getBlock().setType(Material.AIR);
						e.getPlayer().getInventory().addItem(new ItemStack(Material.INK_SACK, 1, (short) 4));
						mainApi.pasteSchematicDelay("lapis", 60, 36, -301, 60);
					}
				}
				else if(e.getBlock().getType().equals(Material.EMERALD_ORE) && (mainApi.isInRegion(e.getBlock(), "jobmineremerald1") || mainApi.isInRegion(e.getBlock(), "jobmineremerald2")
						|| mainApi.isInRegion(e.getBlock(), "jobmineremerald3")))
				{
					if(mainApi.getItemAmount(e.getPlayer(), Material.EMERALD) >= 5)
						e.getPlayer().sendMessage(api.getFullMessage(e.getPlayer()));
					else
					{
						e.getBlock().setType(Material.AIR);
						e.getPlayer().getInventory().addItem(new ItemStack(Material.EMERALD, 1));
						if(mainApi.isInRegion(e.getBlock(), "jobmineremerald1")) mainApi.pasteSchematicDelay("emerald", 164, 36, -359, 60);
						else if(mainApi.isInRegion(e.getBlock(), "jobmineremerald2")) mainApi.pasteSchematicDelay("emerald", 170, 36, -355, 60);
						else if(mainApi.isInRegion(e.getBlock(), "jobmineremerald3")) mainApi.pasteSchematicDelay("emerald", 179, 36, -363, 60);
					}
				}
				else if(e.getBlock().getType().equals(Material.MOSSY_COBBLESTONE) && mainApi.isInRegion(e.getBlock(), "jobminerstone"))
				{
					if(mainApi.getItemAmount(e.getPlayer(), Material.MOSSY_COBBLESTONE) >= 15)
						e.getPlayer().sendMessage(api.getFullMessage(e.getPlayer()));
					else
					{
						e.getBlock().setType(Material.AIR);
						e.getPlayer().getInventory().addItem(new ItemStack(Material.MOSSY_COBBLESTONE, 1));
						mainApi.pasteSchematicDelay("stone", 95, 24, -399, 60);
					}
				}
				else if(e.getBlock().getType().equals(Material.COBBLESTONE) && mainApi.isInRegion(e.getBlock(), "jobminerstone"))
				{
					if(mainApi.getItemAmount(e.getPlayer(), Material.COBBLESTONE) >= 15)
						e.getPlayer().sendMessage(api.getFullMessage(e.getPlayer()));
					else
					{
						e.getBlock().setType(Material.AIR);
						e.getPlayer().getInventory().addItem(new ItemStack(Material.COBBLESTONE, 1));
						mainApi.pasteSchematicDelay("stone", 95, 24, -399, 60);
					}
				}
			}
		}
	}
	
	public void checkLoop()
	{
		MCV.getPlugin().getServer().getScheduler().runTaskTimer(MCV.getPlugin(), new Runnable()
		{
			public void run()
			{
				for(String splayer : api.getAllJoined())
				{
					Player player = MCV.getPlugin().getServer().getPlayerExact(splayer);
					if(mainApi.isInRegion(player, "jobminerplatforma"))
					{
						if(api.getStage(player) == 1 && player.getInventory().contains(api.getPacket(0)))
						{
							double money = 300;
							money += 0.05*(skills.getSkill(player, "miner") - 1)*money;
							player.getInventory().removeItem(api.getPacket(0));
							player.sendMessage(String.format("Ai livrat pachetul si ai primit �2%s$�f! Intoarce-te la �3%s�f pentru a incepe urmatoarea misiune!", money, api.getNPCname()));
							mainApi.getEconomy().depositPlayer(player, money);
							skills.addLivrari(player, "miner", 1);
							api.updateStage(player, 2);
						}
						else if(api.getStage(player) == 3 && player.getInventory().contains(api.getPacket(1)))
						{
							double money = 380;
							money += 0.05*(skills.getSkill(player, "miner") - 1)*money;
							player.getInventory().removeItem(api.getPacket(1));
							player.sendMessage(String.format("Ai livrat pachetul si ai primit �2%s$�f! Intoarce-te la �3%s�f pentru a incepe urmatoarea misiune!", money, api.getNPCname()));
							mainApi.getEconomy().depositPlayer(player, money);
							skills.addLivrari(player, "miner", 1);
							api.updateStage(player, 4);
						}
						else if(api.getStage(player) == 5 && player.getInventory().contains(api.getPacket(2)))
						{
							double money = 410;
							money += 0.05*(skills.getSkill(player, "miner") - 1)*money;
							player.getInventory().removeItem(api.getPacket(2));
							mainApi.removeItemAmount(player, Material.IRON_PICKAXE, 1);
							mainApi.removeItemAmount(player, Material.IRON_SPADE, 1);
							player.sendMessage(String.format("Ai terminat misiunea si ai primit �2%s$�f!", money));
							mainApi.getEconomy().depositPlayer(player, money);
							skills.addLivrari(player, "miner", 1);
							api.updateStage(player, 0);
						}
					}
				}
			}
		}, 10L, 10L);
	}
	
	@EventHandler
	public void onItemThrow(PlayerDropItemEvent e) {
		if(api.getStage(e.getPlayer())>=0) e.setCancelled(true);
	}
	
	@EventHandler
	public void playerKickEvent(PlayerKickEvent e)
	{
		removePlayerFromMiner(e.getPlayer());
	}
	
	@EventHandler
	public void playerQuitEvent(PlayerQuitEvent e)
	{
		removePlayerFromMiner(e.getPlayer());
	}

}
