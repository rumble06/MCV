package me.the_all_nighta.mcv.jobs.fisherman;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.the_all_nighta.mcv.MCV;
import me.the_all_nighta.mcv.MainAPI;

public class FisherAPI {
	
	private ArrayList<String> joinedPlayers = new ArrayList<String>();
	private Location tploc,npcloc;
	private String npcname;
	MainAPI mainApi;
	double[] price = new double[4];
	
	public void setupFisher()
	{
		MCV.Fisher.mainApi = MCV.mainApi;
		mainApi = MCV.mainApi;
		MCV.Fisher.api = MCV.FisherAPI;
		createConfig();
		loadConfig();
		MCV.Fisher.skills = MCV.Skills;
		MCV.Fisher.skills.setupJob("fisherman");
	}
	
	private void createConfig()
	{
		MCV.getPlugin().getConfig().addDefault("jobs.fisherman.teleport.x", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.fisherman.teleport.y", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.fisherman.teleport.z", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.fisherman.teleport.yaw", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.fisherman.teleport.pitch", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.fisherman.npc.name", "Fish Shop");
		MCV.getPlugin().getConfig().addDefault("jobs.fisherman.npc.x", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.fisherman.npc.y", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.fisherman.npc.z", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.fisherman.npc.yaw", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.fisherman.npc.pitch", 0);
		MCV.getPlugin().getConfig().addDefault("jobs.fisherman.price.pastrav", 1);
		MCV.getPlugin().getConfig().addDefault("jobs.fisherman.price.somon", 2);
		MCV.getPlugin().getConfig().addDefault("jobs.fisherman.price.pesteauriu", 3);
		MCV.getPlugin().getConfig().addDefault("jobs.fisherman.price.pestebalon", 4); 
		MCV.getPlugin().getConfig().options().copyDefaults(true);
		MCV.getPlugin().saveConfig();
	}
	
	public void loadConfig()
	{
		tploc = new Location(MCV.getPlugin().getServer().getWorld("world_mcv"), MCV.getPlugin().getConfig().getDouble("jobs.fisherman.teleport.x"),
				MCV.getPlugin().getConfig().getDouble("jobs.fisherman.teleport.y"), MCV.getPlugin().getConfig().getDouble("jobs.fisherman.teleport.z"),
				(float) MCV.getPlugin().getConfig().getDouble("jobs.fisherman.teleport.yaw"), (float) MCV.getPlugin().getConfig().getDouble("jobs.fisherman.teleport.pitch"));
		npcloc = new Location(MCV.getPlugin().getServer().getWorld("world_mcv"), MCV.getPlugin().getConfig().getDouble("jobs.fisherman.npc.x"),
				MCV.getPlugin().getConfig().getDouble("jobs.fisherman.npc.y"), MCV.getPlugin().getConfig().getDouble("jobs.fisherman.npc.z"),
				(float) MCV.getPlugin().getConfig().getDouble("jobs.fisherman.npc.yaw"), (float) MCV.getPlugin().getConfig().getDouble("jobs.fisherman.npc.pitch"));
		npcname = MCV.getPlugin().getConfig().getString("jobs.fisherman.npc.name");
		price[0] = MCV.getPlugin().getConfig().getDouble("jobs.fisherman.price.pastrav");
		price[1] = MCV.getPlugin().getConfig().getDouble("jobs.fisherman.price.somon");
		price[2] = MCV.getPlugin().getConfig().getDouble("jobs.fisherman.price.pesteauriu");
		price[3] = MCV.getPlugin().getConfig().getDouble("jobs.fisherman.price.pestebalon");
	}
	
	public void createNPC()
	{
		Villager npc = (Villager) MCV.getPlugin().getServer().getWorld("world_mcv").spawnEntity(npcloc, EntityType.VILLAGER);
		npc.setCustomName(npcname); npc.setCustomNameVisible(true); npc.setCanPickupItems(false); npc.setAI(false); npc.setInvulnerable(true);
	}
	
	public void addPlayer(Player player)
	{
		joinedPlayers.add(player.getName().toLowerCase());
	}
	
	public void removePlayer(Player player)
	{
		joinedPlayers.remove(player.getName().toLowerCase());
	}
	
	public boolean isJoined(Player player)
	{
		if(joinedPlayers.contains(player.getName().toLowerCase())) return true;
		return false;
	}
	
	public Location getTPloc()
	{
		return tploc;
	}
	
	public String getNPCname()
	{
		return npcname;
	}
	
	public double getPrice(int id)
	{
		if(id>3 || id<0) return 0;
		return price[id];
	}
	
	public Inventory getEmptyShop()
	{
		Inventory shop = MCV.getPlugin().getServer().createInventory(null, 6*9, getNPCname());
		shop.setItem(49, mainApi.setItemTitle(new ItemStack(Material.EMERALD, 1), "�eSELL"));
		for(int i=45;i<=53;i++)
		{
			if(i!=49) shop.setItem(i, mainApi.setItemTitle(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 12), "�f"));
		}
		return shop;
	}

}
