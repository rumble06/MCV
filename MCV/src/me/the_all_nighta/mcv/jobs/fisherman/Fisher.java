package me.the_all_nighta.mcv.jobs.fisherman;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.the_all_nighta.mcv.MCV;
import me.the_all_nighta.mcv.MainAPI;
import me.the_all_nighta.mcv.jobs.Skills;

public class Fisher implements CommandExecutor, Listener {
	
	MainAPI mainApi;
	FisherAPI api;
	Skills skills;
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("spawnvifisher"))
		{
			if(!sender.hasPermission("mcv.jobs.admin")) return true;
			api.createNPC();
			return true;
		}
		
		return false;
	}
	
	public void addPlayerToFisher(Player player)
	{
		if(!player.getGameMode().equals(GameMode.SURVIVAL))
		{
			player.sendMessage("�3[Jobs] �cTrebuie sa fi in gamemode-ul �2survival�c pentru a intra la un job!");
			return;
		}
		for(ItemStack it : player.getInventory().getContents())
		{ 
			if(it!=null)
			{
				player.sendMessage("�3[Jobs] �cTrebuie sa ai inventarul gol cand intrii la un job!");
				return;
			}
		}
		player.teleport(api.getTPloc());
		api.addPlayer(player);
		ItemStack fishingrod = new ItemStack(Material.FISHING_ROD); ItemMeta fishingrodmeta = fishingrod.getItemMeta();
		fishingrodmeta.setUnbreakable(true); fishingrodmeta.setDisplayName("Undita");
		fishingrod.setItemMeta(fishingrodmeta);
		player.getInventory().addItem(fishingrod);
	}
	
	public void removePlayerFromFisher(Player player)
	{
		if(!api.isJoined(player)) return;
		api.removePlayer(player);
		player.getInventory().clear();
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEntityEvent e)
	{
		if(e.getRightClicked().getWorld().equals(MCV.getPlugin().getServer().getWorld("world_mcv")) && mainApi.isInRegion(e.getPlayer(), "jobfisher")
				&& api.isJoined(e.getPlayer()) && e.getHand().equals(EquipmentSlot.HAND) && e.getRightClicked().getName().equals(api.getNPCname()))
		{
			e.setCancelled(true);
			e.getPlayer().openInventory(api.getEmptyShop());
		}
	}
	
	@EventHandler
	public void onPlayerInventoryInteract(InventoryClickEvent e)
	{
		if(e.getWhoClicked().getWorld().equals(MCV.getPlugin().getServer().getWorld("world_mcv")) && api.isJoined((Player) e.getWhoClicked())
				&& e.getInventory().getName().equals(api.getNPCname()))
		{
			Player player = (Player) e.getWhoClicked();
			if(e.getAction().equals(InventoryAction.MOVE_TO_OTHER_INVENTORY)) {
				if(!e.getCurrentItem().getType().equals(Material.RAW_FISH)) e.setCancelled(true);
			}
			if(e.getClickedInventory()!=null && e.getClickedInventory().getName().equals(api.getNPCname()) && e.getCursor() != null)
			{
				if(!(e.getCursor().getType().equals(Material.RAW_FISH) || e.getCursor().getType().equals(Material.AIR))) e.setCancelled(true);
				if(!(e.getCurrentItem().getType().equals(Material.RAW_FISH) || e.getCurrentItem().getType().equals(Material.AIR))) e.setCancelled(true);
				if(e.getCurrentItem().getType().equals(Material.EMERALD))
				{
					double[] totalkg = {0, 0, 0, 0};
					int[] totalfishes = {0, 0, 0, 0};
					for (ItemStack it : e.getClickedInventory().getContents())
					{
						if(it != null && it.getType().equals(Material.RAW_FISH))
						{
							String kgs = it.getItemMeta().getLore().get(0).replaceAll(" kg", "").replaceAll("�f", "");
							double kg = 0;
							try {
								kg = NumberFormat.getInstance(Locale.US).parse(kgs).doubleValue();
							} catch (ParseException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							totalkg[(int) it.getDurability()] += kg*it.getAmount();
							totalfishes[(int) it.getDurability()] += 1*it.getAmount();
							e.getClickedInventory().remove(it);
						}
					}
					double money = (totalkg[0]*api.getPrice(0)) + (totalkg[1]*api.getPrice(1)) + (totalkg[2]*api.getPrice(2)) + (totalkg[3]*api.getPrice(3));
					money += 0.05*(skills.getSkill(player, "fisherman") - 1)*money;
					player.sendMessage(String.format("�3%s�f: Multumesc pentru pesti, poftim �e%s$�f!", api.getNPCname(), money));
					skills.addLivrari(player, "fisherman", totalfishes[0]+totalfishes[1]+totalfishes[2]+totalfishes[3]);
					MCV.getPlugin().getServer().getScheduler().runTaskLater(MCV.getPlugin(), new Runnable() {
						public void run()
						{
							player.closeInventory();
						}
					}, 1L);
					mainApi.getEconomy().depositPlayer(player, money);
				}
			}
		}
	}
	
	@EventHandler
	public void onInventoryDragEvent(InventoryDragEvent e) {
		if(api.isJoined((Player) e.getWhoClicked())&& !e.getOldCursor().getType().equals(Material.RAW_FISH)) {
			for(int a : e.getRawSlots()) {
				if(a<e.getInventory().getSize()) {
					e.setCancelled(true);
					break;
				}
			}
		}
	}
	
	@EventHandler
	public void onPlayerInventoryClose(InventoryCloseEvent e)
	{
		if(e.getPlayer().getWorld().equals(MCV.getPlugin().getServer().getWorld("world_mcv")) && api.isJoined((Player) e.getPlayer())
				&& e.getInventory().getName().equals(api.getNPCname()))
		{
			for(ItemStack it : e.getInventory().getContents())
			{
				if(it!=null && it.getType().equals(Material.RAW_FISH)) e.getPlayer().getInventory().addItem(it);
			}
		}
	}
	
	@EventHandler
	public void onFishingEvent(PlayerFishEvent e)
	{
		if(e.getPlayer().getWorld().equals(MCV.getPlugin().getServer().getWorld("world_mcv")) && mainApi.isInRegion(e.getPlayer(), "jobfisher") && api.isJoined(e.getPlayer())) {
			if(e.getState().equals(PlayerFishEvent.State.IN_GROUND))
			{
				e.getPlayer().sendMessage("�cNu poti sa pescuiesti pe uscat!");
			}
			else if(e.getState().equals(PlayerFishEvent.State.BITE))
			{
				e.getPlayer().sendMessage("�2Un peste a muscat momeala!");
			}
			else if(e.getState().equals(PlayerFishEvent.State.CAUGHT_FISH))
			{
				float kg = (float) (Math.random()*10 + 0.01);
				String kgs = new DecimalFormat("##.##").format(kg);
				double chance = Math.random();
				ItemStack fishIS;
				String fishName;
				if(chance<0.50)
				{
					fishIS = new ItemStack(Material.RAW_FISH, 1, (short) 0);
					fishName = "�bPastrav�f";
				}
				else if(chance<0.8)
				{
					fishIS = new ItemStack(Material.RAW_FISH, 1, (short) 1);
					fishName = "�cSomon�f";
				}
				else if(chance<0.95)
				{
					fishIS = new ItemStack(Material.RAW_FISH, 1, (short) 2);
					fishName = "�6Peste auriu�f";
				}
				else
				{
					fishIS = new ItemStack(Material.RAW_FISH, 1, (short) 3);
					fishName = "�ePeste balon�f";
				}
				fishIS = mainApi.setItemLore(mainApi.setItemTitle(fishIS, fishName), String.format("�f%s kg", kgs), 1);
				e.getPlayer().sendMessage(String.format("Ai prins un %s de �2%skg�f!", fishName, kgs));
				((Item) e.getCaught()).setItemStack(fishIS);
			}
			else if(e.getState().equals(PlayerFishEvent.State.FISHING))
			{
				if(e.getPlayer().getInventory().firstEmpty() == -1)
				{
					e.getPlayer().sendMessage("�cAi inventarul plin, du-te si vinde pestii pentru a mai putea pescuii!");
					e.setCancelled(true);
				}
			}
		}
	}
	
	@EventHandler
	public void onItemThrow(PlayerDropItemEvent e) {
		if(e.getPlayer().getWorld().getName().equals("world_mcv") && mainApi.isInRegion(e.getPlayer(), "jobfisher"))
			e.setCancelled(true);
	}
	
	@EventHandler
	public void onPlayerKick(PlayerKickEvent e)
	{
		removePlayerFromFisher(e.getPlayer());
	}
	
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent e)
	{
		removePlayerFromFisher(e.getPlayer());
	}

}
