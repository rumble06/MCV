package me.the_all_nighta.mcv.jobs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.the_all_nighta.mcv.Database;
import me.the_all_nighta.mcv.MCV;

public class Skills implements CommandExecutor {
	
	HashMap<String, HashMap<String, Integer>> skills = new HashMap<String, HashMap<String, Integer>>();
	HashMap<String, List<Integer>> skillreq = new HashMap<String, List<Integer>>();
	
	public void loadSkillsPlugin()
	{
		saveToDatabaseLoop();
	}
	
	public void createConfig(String job)
	{
		job = job.toLowerCase();
		MCV.getPlugin().getConfig().addDefault(String.format("jobs.%s.skills", job), Arrays.asList(10, 20, 30, 40));
		MCV.getPlugin().getConfig().options().copyDefaults(true);
		MCV.getPlugin().saveConfig();
	}
	
	public void loadConfig(String job)
	{
		job = job.toLowerCase();
		skillreq.put(job, MCV.getPlugin().getConfig().getIntegerList(String.format("jobs.%s.skills", job)));
	}
	
	public void setupJob(String job)
	{
		job = job.toLowerCase();
		try(Connection con = Database.getHikari().getConnection()) {
			PreparedStatement ps = con.prepareStatement("SHOW COLUMNS FROM `JOBS` LIKE ?");
			ps.setString(1, job);
			ResultSet rs = ps.executeQuery();
			if(!rs.next())
			{
				PreparedStatement createColumn = con.prepareStatement("ALTER TABLE `JOBS` ADD `"+job+"` INT DEFAULT 0");
				createColumn.executeUpdate();
			}
			createConfig(job);
			loadConfig(job);
			skills.put(job, new HashMap<String,Integer>());
		} catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void loadSkill(String playerName, String job)
	{
		job = job.toLowerCase();
		playerName = playerName.toLowerCase();
		if(!skills.get(job).containsKey(playerName))
		{
			int abc=0;
			try (Connection con = Database.getHikari().getConnection()){
				PreparedStatement ps = con.prepareStatement("SELECT `"+job+"` FROM `JOBS` WHERE `player` = ?");
				ps.setString(1, playerName);
				ResultSet rs = ps.executeQuery();
				if(rs.next())
				{
					abc = rs.getInt(job);
				}
			} catch(SQLException e)
			{
				e.printStackTrace();
			}
			finally {
				HashMap<String, Integer> hmToAdd = skills.get(job); hmToAdd.put(playerName, abc);
				skills.put(job, hmToAdd);
			}
		}
	}
	
	private void saveToDatabase()
	{
		try (Connection con = Database.getHikari().getConnection()){
			for(String job : skills.keySet())
			{
				PreparedStatement ps = con.prepareStatement("INSERT INTO `JOBS` (`player`, `"+job+"`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `"+job+"` = VALUES(`"+job+"`)");
				HashMap<String, Integer> toWorkWith = skills.get(job);
				for(String playerName : toWorkWith.keySet())
				{
					ps.setString(1, playerName);
					ps.setInt(2, toWorkWith.get(playerName));
					ps.addBatch();
					MCV.getPlugin().getServer().getScheduler().runTask(MCV.getPlugin(), new Runnable(){
						public void run()
						{
							if(MCV.getPlugin().getServer().getPlayerExact(playerName) == null) {
								HashMap<String, Integer> modifier = skills.get(job);
								modifier.remove(playerName);
								skills.put(job, modifier);
							}
						}
					});
				}
				ps.executeBatch();
			}
		} catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	private void saveToDatabaseLoop()
	{
		MCV.getPlugin().getServer().getScheduler().runTaskTimerAsynchronously(MCV.getPlugin(), new Runnable()
		{
			public void run()
			{
				saveToDatabase();
			}
		}, 20L*60*10, 20L*60*10);
	}
	
	public int getSkill(Player player, String job)
	{
		job = job.toLowerCase();
		String playerName = player.getName().toLowerCase();
		loadSkill(playerName, job);
		for(int i=skillreq.get(job).size()-1;i>=0;i--)
		{
			if(skills.get(job).get(playerName) >= skillreq.get(job).get(i)) return i+2;
		}
		return 1;
	}
	
	public int getLivrari(Player player, String job)
	{
		job = job.toLowerCase();
		String playerName = player.getName().toLowerCase();
		loadSkill(playerName, job);
		return skills.get(job).get(playerName);
	}
	
	public void addLivrari(Player player, String job, int numberToAdd)
	{
		job = job.toLowerCase();
		int currentSkill = getSkill(player, job);
		String playerName = player.getName().toLowerCase();
		loadSkill(playerName, job);
		HashMap<String, Integer> hmToAdd = skills.get(job);
		hmToAdd.put(playerName, hmToAdd.get(playerName)+numberToAdd);
		skills.put(job, hmToAdd);
		if(getSkill(player, job) > currentSkill)
		{
			job = job.substring(0, 1).toUpperCase() + job.substring(1);
			player.sendMessage(String.format("Felicitari, ai facut skill %s la %s!", getSkill(player, job), job));
			player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_LAUNCH, SoundCategory.MASTER, 1, 1F);
		}
	}
	
	public ArrayList<String> getAllJobs()
	{
		ArrayList<String> toreturn = new ArrayList<String>();
		toreturn.addAll(skills.keySet());
		return toreturn;
	}
	
	public int getSkillReqS(Player player, String job)
	{
		job = job.toLowerCase();
		if(getSkill(player, job) == 5) return 0;
		if(getSkill(player, job) == 1) return skillreq.get(job).get(getSkill(player, job)-1);
		return skillreq.get(job).get(getSkill(player, job)-1)-skillreq.get(job).get(getSkill(player, job)-2);
	}
	
	public int getLivrariS(Player player, String job)
	{
		job = job.toLowerCase();
		if(getSkill(player, job) == 1) return getLivrari(player, job);
		return getLivrari(player, job) - skillreq.get(job).get(getSkill(player, job)-2);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("skills"))
		{
			Player player = (Player) sender;
			if(args.length==1)
			{
				if(!sender.hasPermission("skills.check"))
				{
					sender.sendMessage("Nu ai acces sa verifici skill-urile altor playeri!");
					return true;
				}
				player = MCV.getPlugin().getServer().getPlayer(args[0]);
				if(player == null) 
				{
					sender.sendMessage("Nu a fost gasit nici un player online cu acest nume!");
					return true;
				}
			}
			sender.sendMessage("Skill-urile lui "+player.getName()+": ");
			int i=1;
			for(String job : getAllJobs())
			{
				job = job.substring(0, 1).toUpperCase() + job.substring(1);
				if(getSkill(player, job) == 5)
				{
					sender.sendMessage(String.format("%s. %s: Skill 5", i, job));
				}
				else
				{
					sender.sendMessage(String.format("%s. %s: Skill %s (%s/%s)", i, job, getSkill(player, job), getLivrariS(player, job), getSkillReqS(player, job)));
				}
				i++;
			}
			
			return true;
		}
		
		
		return false;
	}
	
}
