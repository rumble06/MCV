package me.the_all_nighta.mcv.jobs.farmer;

import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;

import me.the_all_nighta.mcv.MCV;

public class FarmerAPI {
	
	HashMap<String, Double> missionStages = new HashMap<String, Double>();
	HashMap<String, Integer> hayBlockDelivered = new HashMap<String, Integer>();
	String world = "world_mcv";
	Location tploc, fermierloc, mecanicloc;
	String fermierName, mecanicName;
	
	public void setupFarmer()
	{
		loadConfig();
		MCV.Farmer.api = MCV.FarmerAPI;
		MCV.Farmer.mainApi = MCV.mainApi;
		MCV.Farmer.skills = MCV.Skills;
		MCV.Farmer.skills.setupJob("farmer");
	}
	
	public void loadConfig()
	{
		MCV.getPlugin().getConfig().addDefault("jobs.farmer.world", "world_mcv");
		MCV.getPlugin().getConfig().addDefault("jobs.farmer.teleport.x", 0.0);
		MCV.getPlugin().getConfig().addDefault("jobs.farmer.teleport.y", 0.0);
		MCV.getPlugin().getConfig().addDefault("jobs.farmer.teleport.z", 0.0);
		MCV.getPlugin().getConfig().addDefault("jobs.farmer.teleport.yaw", 0.0);
		MCV.getPlugin().getConfig().addDefault("jobs.farmer.teleport.pitch", 0.0);
		MCV.getPlugin().getConfig().addDefault("jobs.farmer.npc.fermier.name", "Fermier");
		MCV.getPlugin().getConfig().addDefault("jobs.farmer.npc.fermier.x", 0.0);
		MCV.getPlugin().getConfig().addDefault("jobs.farmer.npc.fermier.y", 0.0);
		MCV.getPlugin().getConfig().addDefault("jobs.farmer.npc.fermier.z", 0.0);
		MCV.getPlugin().getConfig().addDefault("jobs.farmer.npc.fermier.yaw", 0.0);
		MCV.getPlugin().getConfig().addDefault("jobs.farmer.npc.fermier.pitch", 0.0);
		MCV.getPlugin().getConfig().addDefault("jobs.farmer.npc.mecanic.name", "Mecanic");
		MCV.getPlugin().getConfig().addDefault("jobs.farmer.npc.mecanic.x", 0.0);
		MCV.getPlugin().getConfig().addDefault("jobs.farmer.npc.mecanic.y", 0.0);
		MCV.getPlugin().getConfig().addDefault("jobs.farmer.npc.mecanic.z", 0.0);
		MCV.getPlugin().getConfig().addDefault("jobs.farmer.npc.mecanic.yaw", 0.0);
		MCV.getPlugin().getConfig().addDefault("jobs.farmer.npc.mecanic.pitch", 0.0);
		MCV.getPlugin().getConfig().options().copyDefaults(true);
		MCV.getPlugin().saveConfig();
		
		tploc = new Location(MCV.getPlugin().getServer().getWorld(MCV.getPlugin().getConfig().getString("jobs.farmer.world")), MCV.getPlugin().getConfig().getDouble("jobs.farmer.teleport.x"),
				MCV.getPlugin().getConfig().getDouble("jobs.farmer.teleport.y"), MCV.getPlugin().getConfig().getDouble("jobs.farmer.teleport.z"),
				(float) MCV.getPlugin().getConfig().getDouble("jobs.farmer.teleport.yaw"), (float) MCV.getPlugin().getConfig().getDouble("jobs.farmer.teleport.pitch"));
		fermierloc = new Location(MCV.getPlugin().getServer().getWorld(MCV.getPlugin().getConfig().getString("jobs.farmer.world")), MCV.getPlugin().getConfig().getDouble("jobs.farmer.npc.fermier.x"),
				MCV.getPlugin().getConfig().getDouble("jobs.farmer.npc.fermier.y"), MCV.getPlugin().getConfig().getDouble("jobs.farmer.npc.fermier.z"),
				(float) MCV.getPlugin().getConfig().getDouble("jobs.farmer.npc.fermier.yaw"), (float) MCV.getPlugin().getConfig().getDouble("jobs.farmer.npc.fermier.pitch"));
		mecanicloc = new Location(MCV.getPlugin().getServer().getWorld(MCV.getPlugin().getConfig().getString("jobs.farmer.world")), MCV.getPlugin().getConfig().getDouble("jobs.farmer.npc.mecanic.x"),
				MCV.getPlugin().getConfig().getDouble("jobs.farmer.npc.mecanic.y"), MCV.getPlugin().getConfig().getDouble("jobs.farmer.npc.mecanic.z"),
				(float) MCV.getPlugin().getConfig().getDouble("jobs.farmer.npc.mecanic.yaw"), (float) MCV.getPlugin().getConfig().getDouble("jobs.farmer.npc.mecanic.pitch"));
		fermierName = MCV.getPlugin().getConfig().getString("jobs.farmer.npc.fermier.name"); mecanicName = MCV.getPlugin().getConfig().getString("jobs.farmer.npc.mecanic.name");
	}
	
	
	public void createNPC(int npcc)
	{
		Location npcloc;
		String npcname;
		if(npcc == 1)
		{
			npcloc = fermierloc;
			npcname = fermierName;
		}
		else if(npcc == 2)
		{
			npcloc = mecanicloc;
			npcname = mecanicName;
		}
		else return;
		Villager npc = (Villager) MCV.getPlugin().getServer().getWorld(world).spawnEntity(npcloc, EntityType.VILLAGER);
		npc.setCustomName(npcname); npc.setCustomNameVisible(true); npc.setCanPickupItems(false); npc.setAI(false); npc.setInvulnerable(true);
	}
	
	public String getNPCName(int npcc)
	{
		if(npcc == 1) return fermierName;
		else if(npcc == 2) return mecanicName;
		else return null;
	}
	
	public double getStage(Player player)
	{
		return getStage(player.getName());
	}
	
	public double getStage(String playerName)
	{
		if(missionStages.containsKey(playerName.toLowerCase())) return missionStages.get(playerName.toLowerCase());
		else return -2;
	}
	
	public void updateStage(Player player, double stage)
	{
		updateStage(player.getName(), stage);
	}
	
	public void updateStage(String playerName, double stage)
	{
		if(stage==-2 && missionStages.containsKey(playerName.toLowerCase())) missionStages.remove(playerName.toLowerCase());
		else missionStages.put(playerName.toLowerCase(), stage);
	}
	
	public void removePlayer(Player player)
	{
		updateStage(player.getName(), -2);
		player.getInventory().clear();
	}
	
	
	public Location getTPLoc()
	{
		return tploc;
	}
	
	public int getHayDelivered(Player player)
	{
		return getHayDelivered(player.getName());
	}
	
	public int getHayDelivered(String playerName)
	{
		if(!hayBlockDelivered.containsKey(playerName.toLowerCase())) return 0;
		return hayBlockDelivered.get(playerName.toLowerCase());
	}
	
	public boolean deliverHay(Player player)
	{
		return deliverHay(player.getName());
	}
	
	public boolean deliverHay(String playerName)
	{
		if(hayBlockDelivered.containsKey(playerName.toLowerCase())) hayBlockDelivered.put(playerName.toLowerCase(), hayBlockDelivered.get(playerName.toLowerCase()) + 1);
		else hayBlockDelivered.put(playerName.toLowerCase(), 1);
		if(hayBlockDelivered.get(playerName.toLowerCase()) == 5) 
		{
			hayBlockDelivered.remove(playerName.toLowerCase());
			return true;
		}
		return false;
	}
	
	
}
