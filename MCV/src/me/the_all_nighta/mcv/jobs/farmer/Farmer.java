package me.the_all_nighta.mcv.jobs.farmer;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import me.the_all_nighta.mcv.MCV;
import me.the_all_nighta.mcv.MainAPI;
import me.the_all_nighta.mcv.jobs.Skills;

public class Farmer implements CommandExecutor, Listener {
	
	MainAPI mainApi;
	FarmerAPI api;
	Skills skills;
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("spawnvifarmer"))
		{
			if(!sender.hasPermission("mcv.jobs.owner")) return true;
			if(args.length!=1)
			{
				sender.sendMessage("�3[Jobs] �cCorrect syntax: �2/spawnvifarmer <1/2>�c (1: Farmer, 2: Mecanic)");
				return true;
			}
			int value;
			try{
				value = Integer.parseInt(args[0]);
			} catch(NumberFormatException e)
			{
				sender.sendMessage("�3[Jobs] �cCorrect syntax: �2/spawnvifarmer <1/2>�c (1: Farmer, 2: Mecanic)");
				return true;
			}
			if(value == 1 || value == 2) api.createNPC(value);
			return true;
		}
		return false;
	}
	
	public void removeFromFarmer(Player player)
	{
		if(api.getStage(player) > -2) api.removePlayer(player);
	}
	
	public void addPlayerToFarmer(Player player)
	{
		if(!player.getGameMode().equals(GameMode.SURVIVAL))
		{
			player.sendMessage("�3[Jobs] �cTrebuie sa fi in gamemode-ul �2survival�c pentru a intra la un job!");
			return;
		}
		for(ItemStack it : player.getInventory().getContents())
		{
			if(it!=null){
				player.sendMessage("�3[Jobs] �cTrebuie sa ai inventarul gol cand intrii la un job!");
				return;
			}
		}
		api.updateStage(player, -1);
		player.teleport(api.getTPLoc());
		player.sendMessage(String.format("�3[Jobs] �fAi intrat la jobul �2farmer�f, du-te si dai click dreapta pe NPC-ul [�2%s�f] pentru a incepe prima misiune!", api.getNPCName(1)));
	}
	
	@EventHandler
	public void onPlayerEntityInteract(PlayerInteractEntityEvent e)
	{
		if(mainApi.isInRegion(e.getPlayer(), "jobfarmer") && e.getHand().equals(EquipmentSlot.HAND))
		{
			if(e.getRightClicked().getName().equals(api.getNPCName(1)))
			{
				e.setCancelled(true);
				if(api.getStage(e.getPlayer()) == -1)
				{
					e.getPlayer().sendMessage(String.format("�3%s�f: Te rog adu-mi �264 Wheat�f din lan!", api.getNPCName(1)));
					e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, SoundCategory.MASTER, 1, 1F);
					api.updateStage(e.getPlayer(), 0);
				}
				else if(api.getStage(e.getPlayer()) == 0)
				{
					if(mainApi.getItemAmount(e.getPlayer(), Material.WHEAT) < 64)
					{
						e.getPlayer().sendMessage(String.format("�3%s�f: Mai ai nevoie de inca �2%s Wheat�f pentru a completa misiunea (detii �2%s�f/�264 Wheat�f).",
								api.getNPCName(1), 64-mainApi.getItemAmount(e.getPlayer(), Material.WHEAT), mainApi.getItemAmount(e.getPlayer(), Material.WHEAT)));
						e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, SoundCategory.MASTER, 1, 1F);
					}
					else
					{
						double money = 85;
						money += 0.05*(skills.getSkill(e.getPlayer(), "farmer") - 1)*money;
						mainApi.getEconomy().depositPlayer(e.getPlayer(), money);
						skills.addLivrari(e.getPlayer(), "farmer", 1);
						mainApi.removeItemAmount(e.getPlayer(), Material.WHEAT, 64);
						api.updateStage(e.getPlayer(), 1);
						e.getPlayer().sendMessage(String.format("�3%s�f: Ai fost rasplatit cu �2%s$�f pentru ajutorul tau, du-te la �3%s�f pentru a incepe urmatoarea misiune!",
								api.getNPCName(1), money, api.getNPCName(2)));
						e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, SoundCategory.MASTER, 1, 1F);
					}
				}
				else if(api.getStage(e.getPlayer()) == 4)
				{
					e.getPlayer().sendMessage(String.format("�3%s�f: Du-te si ia �25 baloti de fan�f din caruta pentru a hrani caii!", api.getNPCName(1)));
					api.updateStage(e.getPlayer(), 5);
					e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, SoundCategory.MASTER, 1, 1F);
				}
				else if(api.getStage(e.getPlayer()) == 5)
				{
					e.getPlayer().sendMessage(String.format("�3%s�f: Du-te si ia �25 baloti de fan�f din caruta pentru a hrani caii! (Ai dus �2%s�f/�25 baloti de fan�f)",
							api.getNPCName(1), api.getHayDelivered(e.getPlayer())));
					e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, SoundCategory.MASTER, 1, 1F);
				}
				else if(api.getStage(e.getPlayer()) == 6)
				{
					double money = 65;
					money += 0.05*(skills.getSkill(e.getPlayer(), "farmer") - 1)*money;
					mainApi.getEconomy().depositPlayer(e.getPlayer(), money);
					skills.addLivrari(e.getPlayer(), "farmer", 1);
					e.getPlayer().sendMessage(String.format("�3%s�f: Ai hranit caii cu succes si ai primit �2%s$�f!", api.getNPCName(1), money));
					e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, SoundCategory.MASTER, 1, 1F);
					e.getPlayer().sendMessage(String.format("�3%s�f: Ia galeata asta cu apa, umple-o de la uzina si apoi toarn-o in vasul cu apa pentru a adapa caii!",api.getNPCName(1)));
					e.getPlayer().getInventory().addItem(new ItemStack(Material.BUCKET));
					api.updateStage(e.getPlayer(), 7);
				}
				else if(api.getStage(e.getPlayer()) == 7)
				{
					if(mainApi.getItemAmount(e.getPlayer(), Material.WATER_BUCKET) >= 1)
					{
						e.getPlayer().sendMessage(String.format("�3%s�f: Toarna apa din galeata in vasul cailor!", api.getNPCName(1)));
						e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, SoundCategory.MASTER, 1, 1F);
					}
					else
					{
						e.getPlayer().sendMessage(String.format("�3%s�f: Umple galeata cu apa de la uzina si apoi tarn-o in vasul cu apa pentru a adapa caii!", api.getNPCName(1)));
						e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, SoundCategory.MASTER, 1, 1F);
					}
				}
				else if(api.getStage(e.getPlayer()) == 8)
				{
					double money = 40;
					money += 0.05*(skills.getSkill(e.getPlayer(), "farmer") - 1)*money;
					mainApi.getEconomy().depositPlayer(e.getPlayer(), money);
					skills.addLivrari(e.getPlayer(), "farmer", 1);
					e.getPlayer().sendMessage(String.format("�3%s�f: Ai adapat caii cu succes si ai primit �2%s$�f!", api.getNPCName(1), money));
					e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, SoundCategory.MASTER, 1, 1F);
					e.getPlayer().sendMessage(String.format("�3%s�f: Adu-mi �22 stack-uri de cartofi�f de pe plantatie!", api.getNPCName(1)));
					api.updateStage(e.getPlayer(), 9);
				}
				else if(api.getStage(e.getPlayer()) == 9)
				{
					if(mainApi.getItemAmount(e.getPlayer(), Material.POTATO_ITEM) >= 128)
					{
						double money = 85;
						money += 0.05*(skills.getSkill(e.getPlayer(), "farmer") - 1)*money;
						mainApi.getEconomy().depositPlayer(e.getPlayer(), money);
						skills.addLivrari(e.getPlayer(), "farmer", 1);
						mainApi.removeItemAmount(e.getPlayer(), Material.POTATO_ITEM, 128);
						api.updateStage(e.getPlayer(), -1);
						e.getPlayer().sendMessage(String.format("�3%s�f: Poftim �2%s$�f pentru ajutorul tau!", api.getNPCName(1), money));
						e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, SoundCategory.MASTER, 1, 1F);
					}
					else
					{
						e.getPlayer().sendMessage(String.format("�3%s�f: Ti-am spus sa-mi aduci �22 stack-uri de cartofi�f! Tu ai doar �2%s�f/�128�f.", api.getNPCName(1), mainApi.getItemAmount(e.getPlayer(), Material.POTATO_ITEM)));
						e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, SoundCategory.MASTER, 1, 1F);
					}
				}
			}
			else if(e.getRightClicked().getName().equals(api.getNPCName(2)))
			{
				e.setCancelled(true);
				if(api.getStage(e.getPlayer()) == 1)
				{
					e.getPlayer().sendMessage(String.format("�3%s�f: Ma poti ajuta sa pornesc �2tractorul�f? Te rog repara-l si apoi porneste-l!", api.getNPCName(2)));
					e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, SoundCategory.MASTER, 1, 1F);
					api.updateStage(e.getPlayer(), 2);
				}
				else if(api.getStage(e.getPlayer()) == 2 || api.getStage(e.getPlayer()) == 2.1)
				{
					e.getPlayer().sendMessage(String.format("�3%s�f: Inca nu ai pornit �2tractorul�f, du-te si incearca sa-l pornesti!", api.getNPCName(2)));
					e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, SoundCategory.MASTER, 1, 1F);
				}
				else if(api.getStage(e.getPlayer()) == 3)
				{
					double money = 40;
					money += 0.05*(skills.getSkill(e.getPlayer(), "farmer") - 1)*money;
					skills.addLivrari(e.getPlayer(), "farmer", 1);
					mainApi.getEconomy().depositPlayer(e.getPlayer(), money);
					e.getPlayer().sendMessage(String.format("�3%s�f: Poftim �2%s$�f pentru ca mi-ai reparat tractorul, du-te la �2%s�f pentru a incepe urmatoarea misiune!",
							api.getNPCName(2), money, api.getNPCName(1)));
					e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, SoundCategory.MASTER, 1, 1F);
					api.updateStage(e.getPlayer(), 4);
				}
			}
			else if(e.getRightClicked().getType().equals(EntityType.ITEM_FRAME))
			{
				if(!e.getPlayer().hasPermission("mcv.admin.itemframe") && !mainApi.isInRegion(e.getRightClicked().getLocation(), "jobfarmertractor")) e.setCancelled(true);
				if(api.getStage(e.getPlayer()) == 2.1 && mainApi.isInRegion(e.getRightClicked().getLocation(), "jobfarmertractor"))
				{
					double rand = Math.random();
					if(rand>=0.9)
					{
						api.updateStage(e.getPlayer(), 3);
						e.getPlayer().sendMessage(String.format("�2Ai reusit sa pornesti tractorul, intoarce-te la �3%s�f!", api.getNPCName(2)));
						e.getPlayer().playSound(new Location(MCV.getPlugin().getServer().getWorld("world_mcv"), -586.5, 21, -644.5), Sound.ENTITY_ENDERDRAGON_GROWL, SoundCategory.MASTER, 1, 0.02F);
					}
					else
					{
						e.getPlayer().playSound(new Location(MCV.getPlugin().getServer().getWorld("world_mcv"), -586.5, 21, -644.5), Sound.ENTITY_GHAST_SHOOT, SoundCategory.MASTER, 1, 0.001F);
						e.getPlayer().sendMessage("�cNu ai reusit sa pornesti tractorul, mai incearca!");
					}
				}
				else if(api.getStage(e.getPlayer()) == 3 && mainApi.isInRegion(e.getRightClicked().getLocation(), "jobfarmertractor"))
				{
					e.getPlayer().sendMessage(String.format("Ai pornit deja tractorul, du-te la �3%s�f pentru a continua misiunea!", api.getNPCName(2)));
					e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.BLOCK_GRASS_BREAK, SoundCategory.MASTER, 1, 1F);
				}
				else if(api.getStage(e.getPlayer()) == 2 && mainApi.isInRegion(e.getRightClicked().getLocation(), "jobfarmertractor"))
				{
					e.getPlayer().sendMessage("�cApasa intai pe �3buton�c ca sa repari tractorul!");
					e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.BLOCK_GRASS_BREAK, SoundCategory.MASTER, 1, 1F);
				}
			}
		}
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e)
	{
		if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK) && e.getClickedBlock().getType().equals(Material.STONE_BUTTON) && mainApi.isInRegion(e.getClickedBlock(), "jobfarmertractor"))
		{
			if(api.getStage(e.getPlayer()) == 2)
			{
				e.getPlayer().sendMessage("�2Acum du-te si incearca sa pornesti tractorul invartind de cheie!");
				e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, SoundCategory.MASTER, 1, 1F);
				e.getPlayer().playSound(new Location(MCV.getPlugin().getServer().getWorld("world_mcv"), -586.5, 21, -644.5), Sound.BLOCK_ANVIL_DESTROY, SoundCategory.MASTER, 1, 1F);
				api.updateStage(e.getPlayer(), 2.1);
			}
			else if(api.getStage(e.getPlayer()) == 2.1)
			{
				e.getPlayer().sendMessage("Du-te si incearca sa pornesti tractorul invartind de cheie!");
				e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.BLOCK_GRASS_BREAK, SoundCategory.MASTER, 1, 1F);
			}
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e)
	{
		if(e.getBlock().getLocation().getWorld().getName().equals("world_mcv"))
		{
			if(mainApi.isInRegion(e.getBlock(), "jobfarmerwheat") && e.getBlock().getType().equals(Material.CROPS))
			{
				e.setCancelled(true);
				if(api.getStage(e.getPlayer()) == 0)
				{
					if(mainApi.getItemAmount(e.getPlayer(), Material.WHEAT) >= 64)
					{
						e.getPlayer().sendMessage(String.format("�cAi deja tot �2Wheat-ul�c necesar, du-l �3%s�c!", api.getNPCName(1)));
						e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.BLOCK_ANVIL_LAND, SoundCategory.MASTER, 1, 1F);
						return;
					}
					e.getBlock().setType(Material.AIR);
					e.getPlayer().getInventory().addItem(new ItemStack(Material.WHEAT, 1));
					mainApi.pasteSchematicDelay("farmerwheat", -594, 19, -604, 120);
				}
			}
			else if(mainApi.isInRegion(e.getBlock(), "jobfarmerhaybreak") && e.getBlock().getType().equals(Material.HAY_BLOCK))
			{
				e.setCancelled(true);
				if(api.getStage(e.getPlayer()) == 5)
				{
					if(api.getHayDelivered(e.getPlayer()) + mainApi.getItemAmount(e.getPlayer(), Material.HAY_BLOCK) == 5)
					{
						e.getPlayer().sendMessage("�cAi deja tot �2fanul�c necesar, du-te si da-l cailor!");
						e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.BLOCK_ANVIL_LAND, SoundCategory.MASTER, 1, 1F);
						return;
					}
					e.getPlayer().getInventory().addItem(new ItemStack(Material.HAY_BLOCK, 1));
					e.getBlock().setType(Material.AIR);
					mainApi.pasteSchematicDelay("farmerhay", -533, 19, -660, 30);
					if(api.getHayDelivered(e.getPlayer()) + mainApi.getItemAmount(e.getPlayer(), Material.HAY_BLOCK) == 5)
					{
						e.getPlayer().sendMessage("�2Ai strans tot fanul necesar, du-te si da-l cailor!");
						e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, SoundCategory.MASTER, 1, 1F);
					}
					else
					{
						e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, SoundCategory.MASTER, 1, 1F);
						e.getPlayer().sendMessage(String.format("Mai ai nevoie de �2%s fan�f!", 5-mainApi.getItemAmount(e.getPlayer(), Material.HAY_BLOCK)-api.getHayDelivered(e.getPlayer())));
					}
				}
			}
			else if(mainApi.isInRegion(e.getBlock(), "jobfarmerpotato") && e.getBlock().getType().equals(Material.POTATO))
			{
				e.setCancelled(true);
				if(api.getStage(e.getPlayer()) == 9)
				{
					if(mainApi.getItemAmount(e.getPlayer(), Material.POTATO_ITEM) >= 128)
					{
						e.getPlayer().sendMessage(String.format("�cAi strans deja toti cartofii necesari, intoarce-te la �2%s�c.", api.getNPCName(1)));
						e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.BLOCK_ANVIL_LAND, SoundCategory.MASTER, 1, 1F);
						return;
					}
					e.getPlayer().getInventory().addItem(new ItemStack(Material.POTATO_ITEM, 1));
					e.getBlock().setType(Material.AIR);
					mainApi.pasteSchematicDelay("farmerpotato", -606, 17, -702, 60);
				}
			}
		}
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e)
	{
		if(e.getBlock().getLocation().getWorld().getName().equals("world_mcv"))
		{
			if(mainApi.isInRegion(e.getBlock(), "jobfarmerhayplace") && e.getBlock().getType().equals(Material.HAY_BLOCK))
			{
				// e.setCancelled(true);
				if(api.getStage(e.getPlayer()) == 5)
				{
					if(api.deliverHay(e.getPlayer()))
					{
						api.updateStage(e.getPlayer(), 6);
						e.getPlayer().sendMessage(String.format("�2Intoarce-te la �3%s�2 pentru a-ti primi rasplata si pentru a incepe urmatoarea misiune!", api.getNPCName(1)));
						e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, SoundCategory.MASTER, 1, 1F);
					}
					else
					{
						e.getPlayer().sendMessage(String.format("Mai trebuie sa aduci �2%s�f baloti de fan!", 5-api.getHayDelivered(e.getPlayer())));
					}
					mainApi.removeItemAmount(e.getPlayer(), Material.HAY_BLOCK, 1);
				}
				boolean hayStackFull = true;
				for(int i=0;i<5;i++)
				{
					Location check = new Location(MCV.getPlugin().getServer().getWorld("world_mcv"), -594+i, 19, -668);
					// if(check.getBlock().getType().equals(Material.AIR)) hayStackFull=false;
					if(MCV.getPlugin().getServer().getWorld("world_mcv").getBlockAt(check).getType().equals(Material.AIR)) hayStackFull=false;
				}
				if(hayStackFull)
					for(int i=0;i<5;i++)
					{
						Location replace = new Location(MCV.getPlugin().getServer().getWorld("world_mcv"), -594+i, 19, -668);
						MCV.getPlugin().getServer().getWorld("world_mcv").getBlockAt(replace).setType(Material.AIR);
					}
			}
		}
	}
	
	@EventHandler
	public void onBucketFill(PlayerBucketFillEvent e)
	{
		if(e.getPlayer().getWorld().getName().equals("world_mcv") && mainApi.isInRegion(e.getBlockClicked(), "jobfarmer"))
		{
			e.setCancelled(true);
			if(mainApi.isInRegion(e.getBlockClicked(), "jobfarmerwaterfill") && api.getStage(e.getPlayer()) == 7)
			{
				mainApi.removeItemAmount(e.getPlayer(), Material.BUCKET, 1);
				e.getPlayer().getInventory().addItem(new ItemStack(Material.WATER_BUCKET, 1));
				e.getPlayer().sendMessage("�2Ai luat apa, acum du-te si pune-o in bazinul cailor!");
			}
		}
	}
	
	@EventHandler
	public void onBucketEmpty(PlayerBucketEmptyEvent e)
	{
		if(e.getPlayer().getWorld().getName().equals("world_mcv") && mainApi.isInRegion(e.getBlockClicked(), "jobfarmer"))
		{
			if(mainApi.isInRegion(e.getBlockClicked(), "jobfarmerwaterempty") && api.getStage(e.getPlayer()) == 7)
			{
				e.setCancelled(true);
				mainApi.removeItemAmount(e.getPlayer(), Material.WATER_BUCKET, 1);
				api.updateStage(e.getPlayer(), 8);
				e.getPlayer().sendMessage(String.format("�2Ai adus apa cailor, intoarce-te la �3%s�2.", api.getNPCName(1)));
			}
		}
	}
	
	@EventHandler
	public void onItemThrow(PlayerDropItemEvent e) {
		if(e.getPlayer().getWorld().getName().equals("world_mcv") && mainApi.isInRegion(e.getPlayer(), "jobfarmer"))
			e.setCancelled(true);
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e)
	{
		removeFromFarmer(e.getPlayer());
	}
	
	@EventHandler
	public void onPlayerKick(PlayerKickEvent e)
	{
		removeFromFarmer(e.getPlayer());
	}

}
