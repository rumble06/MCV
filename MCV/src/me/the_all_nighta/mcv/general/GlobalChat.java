package me.the_all_nighta.mcv.general;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.the_all_nighta.mcv.MCV;
import me.the_all_nighta.mcv.MainAPI;

public class GlobalChat implements CommandExecutor {
	
	private static GlobalChat instance;
	
	private GlobalChat() {}
	
	public static GlobalChat getInstance() {
		if(instance == null) instance = new GlobalChat();
		return instance;
	}
	
	MainAPI mainApi;

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("g")) {
			Player player = (Player) sender;
			if(mainApi.getEssentials().getUser(player).isMuted()) {
				player.sendMessage("�3[MCV] �cNu poti folosi aceasta comanda deoarece ai mute!");
				return true;
			}
			if(args.length==0) {
				sender.sendMessage("�3[MCV] �cSintaxa corecta: �a/g <mesaj>�c!");
				return true;
			}
			String message = "";
			for(int i=0;i<args.length-1;i++) message = message+args[i]+" "; message+=args[args.length-1];
			for(Player receiver : MCV.getPlugin().getServer().getOnlinePlayers()) {
				receiver.sendMessage("�2[G]�f"+player.getDisplayName()+"�f: "+message);
			}
			return true;
		}
		
		return false;
	}
	
	public void setupGlobalChat() {
		mainApi = MCV.mainApi;
	}
	
}
