package me.the_all_nighta.mcv.general;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class FreeElytra implements CommandExecutor, Listener {
	
	private static FreeElytra instance;
	
	private FreeElytra() {}
	
	public static FreeElytra getInstance() {
		if(instance == null) instance = new FreeElytra();
		return instance;
	}
	
	ItemStack elytra;
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("elytra") && sender instanceof Player)
		{
			Player player = (Player) sender;
			boolean playerHasElytra = false;
			for(ItemStack is : player.getInventory().getContents()) {
				if(is!=null && is.equals(elytra)) {
					playerHasElytra = true;
					break;
				}
			}
			if(playerHasElytra) {
				if(player.isGliding()) {
					player.sendMessage("�3[MCV] �cNu poti folosi aceasta comanda in timp ce folosesti �aelytra�c!");
					return true;
				}
				if(player.getInventory().getChestplate() != null && player.getInventory().getChestplate().equals(elytra)) player.getInventory().setChestplate(null);
				else if(player.getInventory().getItemInOffHand() != null && player.getInventory().getItemInOffHand().equals(elytra)) player.getInventory().setItemInOffHand(null);
				else player.getInventory().removeItem(elytra);
				player.sendMessage("�3[MCV] �fTi-a fost scoasa �aelytra�f din inventar!");
			} 
			else {
				if(player.getInventory().firstEmpty() == -1) {
					player.sendMessage("�3[MCV] �cNu poti folosi aceasta comanda deoarece ai inventarul plin!");
					return true;
				}
				player.getInventory().addItem(elytra);
				player.sendMessage("�3[MCV] �fTi-a fost adaugat �aelytra�f in inventar!");
			}
			
			return true;
		}
		
		return false;
	}
	
	@EventHandler
	public void onIventoryClickEvent(InventoryClickEvent e) {
		if(e.getAction().equals(InventoryAction.MOVE_TO_OTHER_INVENTORY))
		{
			if(e.getCurrentItem() != null && e.getCurrentItem().equals(elytra) && e.getClickedInventory().equals(e.getWhoClicked().getInventory())) e.setCancelled(true);
		}
		if(e.getCursor() != null && e.getCursor().equals(elytra) && !e.getClickedInventory().equals(e.getWhoClicked().getInventory())) e.setCancelled(true);
	}
	
	@EventHandler
	public void onInventoryDragEvent(InventoryDragEvent e) {
		if(e.getOldCursor() != null && e.getOldCursor().equals(elytra)) {
			for(int a : e.getRawSlots()) {
				if(a<e.getInventory().getSize()) {
					e.setCancelled(true);
					break;
				}
			}
		}
	}
	
	
	@EventHandler
	public void onItemDropEvent(PlayerDropItemEvent e) {
		if(e.getItemDrop().getItemStack().equals(elytra)) e.setCancelled(true);
	}
	
	@EventHandler
	public void onPlayerDeath(EntityDeathEvent e) {
		if(!(e.getEntity() instanceof Player)) return;
		if(e.getDrops().contains(elytra)) e.getDrops().remove(elytra);
	}
	
	public void setupFreeElytra() {
		elytra = new ItemStack(Material.ELYTRA, 1);
		ItemMeta elytraMeta = elytra.getItemMeta();
		elytraMeta.setUnbreakable(true); elytra.setItemMeta(elytraMeta);
	}
	

}
