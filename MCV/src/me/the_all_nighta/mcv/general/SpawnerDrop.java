package me.the_all_nighta.mcv.general;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class SpawnerDrop implements Listener{
	
	private static SpawnerDrop instance;
	
	private SpawnerDrop() {}
	
	public static SpawnerDrop getInstance() {
		if(instance == null) instance = new SpawnerDrop();
		return instance;
	}
	
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void onBlockBreak(BlockBreakEvent e) {
		if(!e.getBlock().getType().equals(Material.MOB_SPAWNER)) return;
		if(!e.getPlayer().getGameMode().equals(GameMode.SURVIVAL)) return;
		if(e.isCancelled()) return;
		double random = Math.random();
		if(random<=0.01) {
			e.getPlayer().sendMessage("�3[MCV] �fAi fost norocos si spawner-ul s-a dropat!");
			e.getPlayer().getWorld().dropItem(e.getBlock().getLocation(), e.getBlock().getState().getData().toItemStack(1));
		}
	}
	
}
