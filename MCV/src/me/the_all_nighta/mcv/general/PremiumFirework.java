package me.the_all_nighta.mcv.general;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import me.the_all_nighta.mcv.MCV;
import me.the_all_nighta.mcv.MainAPI;

public class PremiumFirework implements CommandExecutor, Listener{
	
	private static PremiumFirework instance;
	
	private PremiumFirework() {}
	
	public static PremiumFirework getInstance() {
		if(instance == null) instance = new PremiumFirework();
		return instance;
	}
	
	MainAPI mainApi;
	ItemStack firework;
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("boost") && sender instanceof Player) {
			if(!sender.hasPermission("mcv.elytra.firework")) {
				sender.sendMessage("�3[MCV] �cNu ai acces la aceasta comanda!");
				return true;
			}
			Player player = (Player) sender;
			boolean playerHasFirework = false;
			for(ItemStack is : player.getInventory().getContents()) {
				if(is!=null && is.equals(firework)) {
					playerHasFirework = true;
					break;
				}
			}
			if(playerHasFirework) {
				if(player.getInventory().getItemInOffHand() != null && player.getInventory().getItemInOffHand().equals(firework)) player.getInventory().setItemInOffHand(null);
				else player.getInventory().removeItem(firework);
				player.sendMessage("�3[MCV] �fTi-a fost scos �aFirework-ul infinit�f din inventar!");
			}
			else {
				if(player.getInventory().firstEmpty() == -1) {
					player.sendMessage("�3[MCV] �cNu poti folosi aceasta comanda deoarece ai inventarul plin!");
					return true;
				}
				player.getInventory().addItem(firework);
				player.sendMessage("�3[MCV] �fTi-a fost adaugat �aFirework-ul infinit�f in inventar!");
			}
			
			return true;
		}
		return false;
	}
	
	@EventHandler
	public void onCraftItem(CraftItemEvent e) {
		for(int i=0;i<e.getInventory().getMatrix().length;i++) {
			if(e.getInventory().getMatrix()[i]!=null && e.getInventory().getMatrix()[i].equals(firework)) {
				e.setCancelled(true);
				e.getWhoClicked().sendMessage("�[MCV] �cNu poti crafta folosind �aFirework-ul infinit�c!");
				break;
			}
		}
	}
	
	@EventHandler
	public void onInventoryClickEvent(InventoryClickEvent e) {
		if(e.getAction().equals(InventoryAction.MOVE_TO_OTHER_INVENTORY)) {
			if(e.getCurrentItem().equals(firework) && e.getClickedInventory().equals(e.getWhoClicked().getInventory())) e.setCancelled(true);
		}
		if(e.getCursor().equals(firework) && !e.getClickedInventory().equals(e.getWhoClicked().getInventory())) e.setCancelled(true);
	}
	
	@EventHandler
	public void onInventoryDragEvent(InventoryDragEvent e) {
		if(e.getOldCursor() != null && e.getOldCursor().equals(firework)) {
			for(int a : e.getRawSlots()) {
				if(a<e.getInventory().getSize()) {
					e.setCancelled(true);
					break;
				}
			}
		}
	}
	
	@EventHandler
	public void onPlayerDeath(EntityDeathEvent e) {
		if(!(e.getEntity() instanceof Player)) return;
		if(e.getDrops().contains(firework)) e.getDrops().remove(firework);
	}
	
	@EventHandler
	public void onItemDropEvent(PlayerDropItemEvent e) {
		if(e.getItemDrop().getItemStack().equals(firework)) e.setCancelled(true);
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if(!e.getPlayer().hasPermission("mcv.elytra.firework")) return;
		if(e.getItem()!=null && e.getItem().equals(firework)) {
			if(e.getAction().equals(Action.RIGHT_CLICK_AIR)) {
				if(e.getPlayer().isGliding()) e.getItem().setAmount(2);
				else e.setCancelled(true);
			}
			else if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) e.setCancelled(true);
		}
	}
	
	public void setupPremiumFirework() {
		mainApi = MCV.mainApi;
		firework = mainApi.setItemTitle(new ItemStack(Material.FIREWORK, 1), "�6[PREMIUM]�f Firework");
	}
}
