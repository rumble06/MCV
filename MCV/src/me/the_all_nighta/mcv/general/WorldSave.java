package me.the_all_nighta.mcv.general;

import org.bukkit.World;

import me.the_all_nighta.mcv.MCV;

public class WorldSave {
	
	private static WorldSave instance;
	
	private WorldSave() {}
	
	public static WorldSave getInstance() {
		if(instance == null) instance = new WorldSave();
		return instance;
	}
	
	public void runLoop() {
		
		MCV.getPlugin().getServer().getScheduler().runTaskTimer(MCV.getPlugin(), new Runnable() {
			@Override
			public void run() {
				for(World w : MCV.getPlugin().getServer().getWorlds()) {
					w.save();
				}
			}
		}, 20L*60, 20L*60*5);
		
	}
	
}
