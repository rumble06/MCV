package me.the_all_nighta.mcv.general;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;

import me.the_all_nighta.mcv.MCV;

public class StopProxy implements CommandExecutor, Listener {
	
	private static StopProxy instance;
	
	private StopProxy() {}
	
	public static StopProxy getInstance() {
		if(instance == null) {
			instance = new StopProxy();
		}
		return instance;
	}
	
	boolean protection = true; String kickmsg="",nopermissionsmsg="";
	
	public void setupPlugin() {
		setupConfig();
		loadConfig();
	}
	
	private void setupConfig() {
		MCV.getPlugin().getConfig().addDefault("proxy.protection", Boolean.valueOf(true));
		MCV.getPlugin().getConfig().addDefault("proxy.KickMSG", "IP-ul tau a fost detectat ca fiind proxy!");
		MCV.getPlugin().getConfig().addDefault("proxy.NoPermissionsMSG", "Nu ai acces la aceasta comanda!");
		MCV.getPlugin().getConfig().options().copyDefaults(true);
		MCV.getPlugin().saveConfig();
		MCV.getPlugin().reloadConfig();
	}
	
	private void loadConfig() {
		MCV.getPlugin().reloadConfig();
		protection = MCV.getPlugin().getConfig().getBoolean("proxy.protection");
		kickmsg = MCV.getPlugin().getConfig().getString("proxy.KickMSG");
		nopermissionsmsg = MCV.getPlugin().getConfig().getString("proxy.noPermissionsMSG");
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void onJoin(AsyncPlayerPreLoginEvent e) {
		if(!protection) return;
		try {
			URL url = new URL("http://stpprx2server.altervista.org/algo3.php?ip="+e.getAddress().getHostAddress());
			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
			String result = in.readLine();in.close();
			String address = e.getAddress().getHostAddress();
			if(result.contains("si_pr")) {
				MCV.getPlugin().getServer().getScheduler().runTask(MCV.getPlugin(), new Runnable() {
					@Override
					public void run() {
						MCV.getPlugin().getLogger().info("Proxy "+address+" tried to login!");
					}
				});
				e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_BANNED, ChatColor.RED + kickmsg);
			}
			else {
				MCV.getPlugin().getServer().getScheduler().runTask(MCV.getPlugin(), new Runnable() {
					@Override
					public void run() {
						MCV.getPlugin().getLogger().info("IP "+address+" is not a proxy!");
					}
				});
			}
		} catch(MalformedURLException localMalformedURLException) {localMalformedURLException.printStackTrace();} catch(IOException localIOException) {localIOException.printStackTrace();}
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("sproxy")) {
			if(!sender.hasPermission("stopproxy.admin")) {
				sender.sendMessage("�3[MCV] �cNu ai acces la aceasta comanda!");
				return true;
			}
			if(args.length==0 || (args.length==1 && args[0].equalsIgnoreCase("help"))) {
				sender.sendMessage(ChatColor.GOLD+"------------STOPPROXY----------------");
				sender.sendMessage("");
				sender.sendMessage(ChatColor.GOLD+"Plugin remade by The_All_Nighta");
				sender.sendMessage("");
				sender.sendMessage(ChatColor.GREEN+"/sproxy protection [ON/OFF] - Enable or Disable protection");
				sender.sendMessage(ChatColor.GREEN+"/sproxy getconfig - Check protection status");
				sender.sendMessage(ChatColor.GOLD+"------------------------------------------");
				return true;
			}
			if(args.length>=1) {
				if(args[0].equalsIgnoreCase("protection")) {
					if(args.length==1) return true;
					if(args[1].equalsIgnoreCase("on")) {
						MCV.getPlugin().getConfig().set("proxy.protection", Boolean.valueOf(true));
						MCV.getPlugin().saveConfig();
						loadConfig();
						sender.sendMessage(ChatColor.GOLD+"[StopProxy] Protection: "+ChatColor.GREEN+"Enabled!");
						return true;
					}
					if(args[1].equalsIgnoreCase("off")) {
						MCV.getPlugin().getConfig().set("proxy.protection", Boolean.valueOf(false));
						MCV.getPlugin().saveConfig();
						loadConfig();
						sender.sendMessage(ChatColor.GOLD+"[StopProxy] Protection: "+ChatColor.RED+"Disabled!");
						return true;
					}
					sender.sendMessage(ChatColor.GREEN+"/sproxy protection [ON/OFF] - Enable or Disable protection");
					return true;
				}
				else if(args[0].equalsIgnoreCase("getconfig")) {
					sender.sendMessage(ChatColor.GOLD+"------------STOPPROXY-----------------");
					sender.sendMessage("");
					sender.sendMessage(ChatColor.GOLD+"Protection: "+(protection ? ChatColor.GREEN+"Enabled" : ChatColor.RED+"Disabled"));
					sender.sendMessage("");
					sender.sendMessage(ChatColor.GOLD+"------------------------------------------");
					return true;
				}
			}
			return true;
		}
		
		return false;
	}
	
}
