package me.the_all_nighta.mcv;

import com.zaxxer.hikari.HikariDataSource;

public class Database {
	
	private static HikariDataSource hikari;
	
	public static void connectToDB() {
		hikari = new HikariDataSource();
		hikari.setMaximumPoolSize(10);
		hikari.setJdbcUrl("jdbc:mysql://localhost:3306/minecraft_76649");
		hikari.setUsername("minecraft_76649");
		hikari.setPassword("server closed");
		hikari.addDataSourceProperty("cachePrepStmts", "true");
		hikari.addDataSourceProperty("prepStmtCacheSize", "250");
		hikari.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
	}
	
	public static void disconnectFromDB() {
		if(hikari!=null) hikari.close();
	}
	
	public static HikariDataSource getHikari() {
		return hikari;
	}
	
}
